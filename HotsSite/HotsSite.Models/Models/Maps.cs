﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace HotsSite.Models.Models
{
    public class Maps
    {
        public int MapId { get; set; }

        [Required(ErrorMessage = "What is the name of this map?")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Tell us about this map.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "What does this map look like?")]
        public Img Img { get; set; }
    }
}
