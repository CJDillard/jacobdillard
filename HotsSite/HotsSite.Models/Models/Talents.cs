﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Models
{
    public class Talents
    {
        public int Id { get; set; }

        [Required(ErrorMessage = ("What is the name of this Talent?"))]
        public string Name { get; set; }
    
        [Required(ErrorMessage = ("Tell us about this talent!"))]
        public string Description { get; set; }
    }
}