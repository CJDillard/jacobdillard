﻿using HotsSite.Models.Enums;
using HotsSite.Models.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Models
{
    public class Hero
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "What is the characters name?")]
        public string Name { get; set; }        

        [Required(ErrorMessage = "What is this characters talents?")]
        public List<Talents> Talents {get; set;}

        [Required(ErrorMessage = "What is this characters heroics?")]
        public List<Heroics> Heroics { get; set; }

        [Required(ErrorMessage = "What does this character look like?")]
        public Img Img { get; set; }

        [Required(ErrorMessage = "What is this characters passive?")]
        public string Passive { get; set; }

        [Required(ErrorMessage = "What game is this character from?")]
        public Game Game { get; set; }

        [Required(ErrorMessage = "What spec is this character?")]
        public Spec Spec { get; set; }

        [Required(ErrorMessage = "Tell us about this character.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "How much does this character cost?")]
        public decimal Price { get; set; }
    }
}
