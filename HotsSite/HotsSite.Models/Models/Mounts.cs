﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Models.Models
{
    public class Mounts
    {
        public int MountId { get; set; }

        [Required(ErrorMessage = "What is this mounts name?")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Tell us about this mount.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "What does this mount look like?")]
        public Img Img { get; set; }
    }
}
