﻿using HotsSite.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Models.Responses
{
    public class HeroicResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public Heroics Heroic { get; set; }
    }
}
