﻿using HotsSite.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Models.Responses
{
    public class MountResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public Mounts Mount { get; set; }
    }
}
