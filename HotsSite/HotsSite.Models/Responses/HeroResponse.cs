﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Models.Responses
{
    public class HeroResponse
    {
        public  bool Success { get; set; }

        public string Message { get; set; }

        public Hero Hero { get; set; }
    }
}
