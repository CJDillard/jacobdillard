﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Models.Enums
{
    public enum Game
    {
        Warcraft,
        Starcraft,
        Diablo,
        Overwatch
    }
}
