﻿using HotsSite.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Factories
{
    public class MapFactory
    {
        public static IMapRepository CreateMapRepository()
        {
            IMapRepository repo;

            string mode = ConfigurationManager.AppSettings["Mode"].ToString();
            switch (mode)
            {
                case "Test":
                    repo = new InMemMapRepo();
                    break;
                case "Prod":
                    repo = new MapDBRepo();
                    break;
                default:
                    throw new Exception($"{mode} is not a fuctional mode. Try another.");
            }
            return repo;
        }
    }
}
