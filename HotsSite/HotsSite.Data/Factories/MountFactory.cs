﻿using HotsSite.Data.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Factories
{
    public class MountFactory
    {
        public static IMountRepository CreateMountRepository()
        {
            IMountRepository repo;

            string mode = ConfigurationManager.AppSettings["Mode"].ToString();
            switch (mode)
            {
                case "Test":
                    repo = new InMemMountRepo();
                    break;
                case "Prod":
                    repo = new MountDBRepo();
                    break;
                default:
                    throw new Exception($"{mode} is not a fuctional mode. Try another.");
            }
            return repo;
        }
    }
}
