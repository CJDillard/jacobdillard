﻿using HotsSite.Data.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Factories
{
    public class HeroFactory
    {
        public static IHeroRepository CreateHeroRepository()
        {
            IHeroRepository repo;

            string mode = ConfigurationManager.AppSettings["Mode"].ToString();
            switch (mode)
            {
                case "Test":
                    repo = new InMemHeroRepo();
                    break;
                case "Prod":
                    repo = new HeroDBRepo();
                    break;
                default:
                    throw new Exception($"{mode} is not a fuctional mode. Try another.");
            }
            return repo;
        }
    }
}
