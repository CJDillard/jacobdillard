﻿using HotsSite.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Factories
{
   public class HeroicsFactory
    {
        public static IHeroicRepository CreateHeroRepository()
        {
            IHeroicRepository repo;

            string mode = System.Configuration.ConfigurationManager.AppSettings["Mode"].ToString();
            switch (mode)
            {
                case "Test":
                    repo = new InMemHeroicRepo();
                    break;
                case "Prod":
                    repo = new HeroicDBRepo();
                    break;
                default:
                    throw new Exception($"{mode} is not a fuctional mode. Try another.");
            }
            return repo;
        }
}
