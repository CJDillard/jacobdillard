﻿using HotsSite.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Interface
{
    public interface IMountRepository
    {
        Mounts GetMountById(int id);

        List<Mounts> GetAllMounts();

        void AddMount(Mounts mountToAdd);

        void RemoveMount(Mounts mountToRemove);

        void EditMount(Mounts mountToEdit);
    }
}
