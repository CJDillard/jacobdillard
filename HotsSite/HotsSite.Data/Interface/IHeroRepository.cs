﻿using HotsSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Interface
{
    public interface IHeroRepository
    {
        Hero GetHeroById(int id);

        List<Hero> GetAllHeros();

        void AddHero(Hero heroToAdd);

        void RemoveHero(Hero heroToRemove);

        void EditHero(Hero heroToEdit);
    }
}
