﻿using HotsSite.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Interface
{
    public interface IMapRepository
    {
        Maps GetMapById(int id);

        List<Maps> GetAllMaps();

        void AddMap(Maps mapToAdd);

        void RemoveMap(Maps mapToRemove);

        void EditMap(Maps mapToEdit);
    }
}
