﻿using HotsSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Interface
{
    public interface ITalentRepository
    {
        List<Talents> GetAllTalents();

        Talents GetTalentById(int id);

        void AddTalent(Talents talentToAdd, int heroId);

        void RemoveTalent(Talents talentToRemove);

        void EditTalent(Talents talentToEdit);
    }
}
