﻿using HotsSite.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.Data.Interface
{
    public interface IHeroicRepository
    {
        Heroics GetHeroicById(int id);

        List<Heroics> GetAllHeroics();

        void AddHeroic(Heroics heroToAdd);

        void RemoveHeroic(Heroics heroToRemove);

        void EditHeroic(Heroics heroToEdit);
    }
}
