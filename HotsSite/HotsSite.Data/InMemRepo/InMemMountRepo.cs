﻿using System;
using System.Collections.Generic;
using HotsSite.Data.Interface;
using HotsSite.Models.Models;
using System.Linq;

namespace HotsSite.Data.Factories
{
    public class InMemMountRepo : IMountRepository
    {
        private static List<Mounts> _mounts;

        public InMemMountRepo()
        {
            if (_mounts == null)
            {
                _mounts = new List<Mounts>()
                {
                    new Mounts()
                    {
                       MountId = 1,
                       Name = "Horde Wolf",
                       Description = "Bred for war and boy is it ready",
                       //Img =
                    }
                };
            }
        }

        public void AddMount(Mounts mountToAdd)
        {
            _mounts.Add(mountToAdd);
        }

        public void EditMount(Mounts mountToEdit)
        {
            var mount = _mounts.FirstOrDefault(m => m.MountId == mountToEdit.MountId);
            _mounts.Remove(mount);
            _mounts.Add(mountToEdit);
        }

        public List<Mounts> GetAllMounts()
        {
            return _mounts;
        }

        public Mounts GetMountById(int id)
        {
            return _mounts.FirstOrDefault(m => m.MountId == id);
        }

        public void RemoveMount(Mounts mountToRemove)
        {
            var result = _mounts.FirstOrDefault(m => m.MountId == mountToRemove.MountId);
            _mounts.Remove(result);
        }
    }
}