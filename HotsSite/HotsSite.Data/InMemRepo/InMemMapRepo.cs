﻿using System;
using System.Collections.Generic;
using HotsSite.Data.Interface;
using HotsSite.Models.Models;
using System.Linq;

namespace HotsSite.Data.Factories
{
    public class InMemMapRepo : IMapRepository
    {
        private static List<Maps> _maps;

        public InMemMapRepo()
        {
            if (_maps == null)
            {
                _maps = new List<Maps>()
                {
                    new Maps()
                    {
                        MapId = 1,
                        Name = "Dragon Shrine",
                        Description = "Collect shrines to turn into a dragon.",
                        //Img =
                    }
                };
            }
        }

        public void AddMap(Maps mapToAdd)
        {
            _maps.Add(mapToAdd);
        }

        public void EditMap(Maps mapToEdit)
        {
            var map = _maps.FirstOrDefault(m => m.MapId == mapToEdit.MapId);
            _maps.Remove(map);
            _maps.Add(mapToEdit);
        }

        public List<Maps> GetAllMaps()
        {
            return _maps;
        }

        public Maps GetMapById(int id)
        {
            return _maps.FirstOrDefault(m => m.MapId == id);
        }

        public void RemoveMap(Maps mapToRemove)
        {
            var result = _maps.FirstOrDefault(m => m.MapId == mapToRemove.MapId);
            _maps.Remove(result);
        }
    }
}