﻿using System;
using System.Collections.Generic;
using HotsSite.Data.Interface;
using HotsSite.Models;

namespace HotsSite.Data.Factories
{
    public class InMemTalentRepo : ITalentRepository
    {
        public void AddTalent(Talents talentToAdd, int heroId)
        {
            throw new NotImplementedException();
        }

        public void EditTalent(Talents talentToEdit)
        {
            throw new NotImplementedException();
        }

        public List<Talents> GetAllTalents()
        {
            throw new NotImplementedException();
        }

        public Talents GetTalentById(int id)
        {
            throw new NotImplementedException();
        }

        public void RemoveTalent(Talents talentToRemove)
        {
            throw new NotImplementedException();
        }
    }
}