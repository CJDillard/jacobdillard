﻿using System;
using System.Collections.Generic;
using HotsSite.Data.Interface;
using HotsSite.Models;
using HotsSite.Models.Enums;
using System.Linq;
using HotsSite.Models.Models;

namespace HotsSite.Data.Factories
{
    public class InMemHeroRepo : IHeroRepository
    {

        private static List<Hero> _heroes;

        public InMemHeroRepo()
        {
            if (_heroes == null)
            {
                _heroes = new List<Hero>()
                {
                    new Hero
                    {
                        Id = 1,
                        Name = "Thrall",
                        Game = Game.Warcraft,
                        Description = "As a child, Thrall served the cruel Aedelas Blackmoore as a slave. After gaining his freedom, he guided the liberated orcs to a land of their own and worked to recover their ancient traditions. Now and forever he is the Warchief of the Horde.",
                        Spec = Spec.DPS,
                        Price = Convert.ToDecimal("10000"),
                        Talents = new List<Talents>
                        {
                            new Talents()
                            {
                                Id = 1,
                                Name = "Chain Lighting",
                                Description = "Does what it says",
                            },

                            new Talents()
                            {
                                Id = 2,
                                Name = "Feral Spirit",
                                Description = "Fire Wolf"

                            },

                            new Talents()
                            {
                                Id = 3,
                                Name = "Windfury",
                                Description = "Doomhammer go smash"
                            },                         
                        },

                        Heroics = new List<Heroics>
                        {
                            new Heroics()
                            {
                                Id = 1,
                                Name = "Sundering",
                                Description = "Giant lava earth quake but in a straightline"
                            },

                            new Heroics()
                            {
                                Id = 2,
                                Name = "Earthquake",
                                Description = "Giant frostwolf symbol appears. Dont judge hes very proud of it"
                            }
                        },

                        Passive = "Frostwolf Resilience",
                    },
                };
            }
        }
        public void AddHero(Hero heroToAdd)
        {
            _heroes.Add(heroToAdd);
        }

        public void EditHero(Hero heroToEdit)
        {
            var hero = _heroes.FirstOrDefault(h => h.Id == heroToEdit.Id);
            _heroes.Remove(hero);
            _heroes.Add(heroToEdit);
        }

        public List<Hero> GetAllHeros()
        {
            return _heroes;
        }

        public Hero GetHeroById(int id)
        {
            return _heroes.FirstOrDefault(h => h.Id == id);
        }

        public void RemoveHero(Hero heroToRemove)
        {
            var result = _heroes.FirstOrDefault(h => h.Id == heroToRemove.Id);
            _heroes.Remove(result);
        }
    }
}