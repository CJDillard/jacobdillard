﻿using HotsSite.Data.Factories;
using HotsSite.Models;
using HotsSite.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsSite.BLL.Managers
{
    public class HeroManager
    {
        public Hero GetHeroById(int id)
        {
            HeroResponse response = new HeroResponse();

            var repo = HeroFactory.CreateHeroRepository();
            var hero = repo.GetHeroById(id);

            if (hero != null)
            {
                response.Success = true;
                response.Message = "It worked!";
                response.Hero = hero;
            }
            else
            {
                response.Success = false;
                response.Message = "Hero not found!";
            }
            return hero;
        }

        public List<Hero> GetAllHeros()
        {
            var repo = HeroFactory.CreateHeroRepository();
            var hero = repo.GetAllHeros();
            return hero;
        }
    }
}
