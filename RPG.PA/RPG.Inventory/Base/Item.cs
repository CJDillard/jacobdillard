﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Inventory.Base
{
    public abstract class Item
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int SlotSpace { get; set; }
        public ItemType Type { get; protected set; }

        public override string ToString()
        {
            return $"{Name} - {Description}";
        }
    }
}
