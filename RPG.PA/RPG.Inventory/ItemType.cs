﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Inventory
{
    public enum ItemType
    {
        Clothing,
        Container,
        Food,
        Weapon,
        Ammo
    }
}
