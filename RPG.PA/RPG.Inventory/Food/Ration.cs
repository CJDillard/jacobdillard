﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Food
{
    public class Ration : Foods
    {
        public Ration()
        {
            Name = "Ration";
            Description = "Nutrition packed bars useful for every Ranger and then some.";
            SlotSpace = 1;
            HealingValue = 10;
            Type = ItemType.Food;
        }
    }
}
