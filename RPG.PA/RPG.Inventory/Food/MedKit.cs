﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Food
{
   public class MedKit : Foods
    {
       public MedKit()
       {
           Name = "A Standard MedKit";
           Description = "A Med-Kit will all the essential healing components";
           SlotSpace = 2;
           HealingValue = 20;
           Type = ItemType.Food;
       }
    }
}
