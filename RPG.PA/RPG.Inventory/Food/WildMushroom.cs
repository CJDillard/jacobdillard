﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Food
{
   public  class WildMushroom : Foods
    {
       public WildMushroom()
       {
           Name = "A Wild Mushroom";
           Description = "You'd think they would be harmeful but apprently the radiation does wonders";
           SlotSpace = 1;
           HealingValue = 3;
           Type = ItemType.Food;
       }
    }
}
