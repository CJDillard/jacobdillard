﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Food
{
    public class Bandage : Foods
    {
        public Bandage()
        {
            Name = "A cloth bandage";
            Description = "A gauze bandage to stop most bleeding";
            SlotSpace = 1;
            HealingValue = 15;
            Type = ItemType.Food;
        }
    }
}
