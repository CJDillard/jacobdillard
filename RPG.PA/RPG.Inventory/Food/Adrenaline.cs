﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Food
{
    public class Adrenaline : Foods
    {
        public Adrenaline()
        {
            Name = "A shot of Adrenaline";
            Description = "Provides a temporary health boost";
            SlotSpace = 2;
            HealingValue = 10;
            Type = ItemType.Food;
        }
    }
}
