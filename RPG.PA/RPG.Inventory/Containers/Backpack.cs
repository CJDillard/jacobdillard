﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Containers
{
    public class Backpack : Container
    {
        public Backpack(int capacity) : base(capacity)
        {
        }
    }
}
