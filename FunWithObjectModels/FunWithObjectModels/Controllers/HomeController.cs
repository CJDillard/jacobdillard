﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunWithObjectModels.Models;

namespace FunWithObjectModels.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AnotherPage(int id)
        {
            var teams = new List<Teams>()
            {
                new Teams() {id = 1, City = "Cleveland", Name = "Indians"},
                new Teams() {id = 2, City = "Chicago", Name = "Cubs"}
            };

            var team = teams.FirstOrDefault((t => t.id == id)); 

            return View(team);
        }
    }
}