﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunWithObjectModels.Models
{
    public class Teams
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
    }
}