﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodeReviewWeek7.Models
{
    public class Application
    {
        [Required(ErrorMessage = "Please enter your name, summoner!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "How will we add you, summoner?")]
        public string SummonerName { get; set; }

        [Required(ErrorMessage = "Please add your email!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Many JA's text outside of league! You'll want to get in on that!")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Can't forget your favorite champ!")]
        public string FavoriteChampion { get; set; }

        [Required(ErrorMessage = "We need to know this!")]
        public string PreferredGameType { get; set; }

        [Required(ErrorMessage = "Sorry Christian! You have to choose one!")]
        public bool? DCS { get; set; }
    }
}