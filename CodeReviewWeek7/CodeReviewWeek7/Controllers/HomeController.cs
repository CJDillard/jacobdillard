﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodeReviewWeek7.Models;

namespace CodeReviewWeek7.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ApplicationForm()
        {
            return View(new Application());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApplicationForm(Application app)
        {
            if (ModelState.IsValid)
            {
                return View("Thanks", app);
            }
            else
            {
                return View(app);
            }
        }
    }
}