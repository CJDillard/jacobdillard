﻿$(document)
    .ready(function() {
        $('#applicationForm')
            .validate({
                rules: {
                    Name: {
                        required: true
                    },
                    SummonerName: {
                        required: true
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    Phone: {
                        required: true,
                        phoneUS: true
                    },
                    FavoriteChampion: {
                        required: true
                    },
                    PreferredGameType: {
                        required: true
                    },
                    DCS: {
                        required: true
                    }
                },
                messages: {
                    Name: {
                        required: "Please enter your name, summoner!"
                    },
                    SummonerName: {
                        required: "How will we add you without your summoner name?"
                    },
                    Email: {
                        required: "Please don't forget your email!",
                        email: "You obviously know what an email looks like"
                    },
                    Phone: {
                        required: "Some JA's text otuside of league! You'll want to get in on this!",
                        phoneUS: "We aren't international yet! Has to be a US phone number!"
                    },
                    FavoriteChampion: {
                        required: "Can't forget your favorite champ!"
                    },
                    PreferredGameType: {
                        required: "We need to know this!"
                    },
                    DCS: {
                        required: "Nice try Christian! You have to pick one ;)"
                    }
                    
                }
    
            })
    });