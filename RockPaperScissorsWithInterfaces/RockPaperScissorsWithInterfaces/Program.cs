﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissorsWithInterfaces.Rng;

namespace RockPaperScissorsWithInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            Game game;

            Console.WriteLine("Select your mode: (E)asy, (N)ormal, (T)wo Player");
            input = Console.ReadLine();

            if (input.ToUpper() == "E")
                game = new Game(new WeightedChoicePicker());
            else if (input.ToUpper() == "N")
                game = new Game(new RandomChoicePicker());
            else
                game = new Game(new TwoPlayerPicker());

            do
            {
                Choice userChoice = GetUserChoice();
                var result = game.PlayRound(userChoice);

                ProcessResult(result);

                Console.WriteLine("Press Q to quit");
                input = Console.ReadLine();
            } while (input.ToUpper() != "Q");
        }

        private static void ProcessResult(MatchResult result)
        {
            Console.WriteLine("You picked {0}, your opponent picked {1}", Enum.GetName(typeof(Choice), 
                result.UserChoice),
                Enum.GetName(typeof(Choice), result.OpponentChoice));

            switch (result.Result)
            {
                case GameResult.Tie:
                    Console.WriteLine("You tied!");
                    break;
                case GameResult.Lose:
                    Console.WriteLine("You lose!");
                    break;
                default:
                    Console.WriteLine("You won!");
                    break;
            }
        }

        private static Choice GetUserChoice()
        {
            Console.Clear();
            Console.WriteLine("Player 1, Enter a choice (R)ock, (P)aper, (S)cissors");
            string input = Console.ReadLine();

            switch (input.ToUpper())
            {
                case "P":
                    return Choice.Paper;
                case "S":
                    return Choice.Scissors;
                default:
                    return Choice.Rock;
            }
        }
    }
}
