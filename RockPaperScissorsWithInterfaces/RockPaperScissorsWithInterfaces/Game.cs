﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsWithInterfaces
{
    public class Game
    {
        private IChoiceSelector picker;

        public Game(IChoiceSelector choiceSelector)
        {
            picker = choiceSelector;
        }

        public MatchResult PlayRound(Choice userChoice)
        {
            var result = new MatchResult();
            result.UserChoice = userChoice;
            //WeightedChoicePicker picker = new WeightedChoicePicker();
            result.OpponentChoice = picker.GetOpponentChoice();

            if (result.OpponentChoice == result.UserChoice)
            {
                result.Result = GameResult.Tie;
                return result;
            }

            if (result.OpponentChoice == Choice.Rock && result.UserChoice == Choice.Scissors || 
                result.OpponentChoice == Choice.Scissors && result.UserChoice == Choice.Paper ||
                result.OpponentChoice == Choice.Paper && result.UserChoice == Choice.Rock)
            {
                result.Result = GameResult.Lose;
                return result;
            }

            result.Result = GameResult.Win;
            return result;
        }
    }
}
