﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsWithInterfaces.Rng
{
    public class TwoPlayerPicker : IChoiceSelector
    {
        public Choice GetOpponentChoice()
        {
            Console.Clear();
            Console.WriteLine("Player 2, Enter a choice (R)ock, (P)aper, (S)cissors");
            string input = Console.ReadLine();

            switch (input.ToUpper())
            {
                case "P":
                    return Choice.Paper;
                case "S":
                    return Choice.Scissors;
                default:
                    return Choice.Rock;
            }
        }
    }
}
