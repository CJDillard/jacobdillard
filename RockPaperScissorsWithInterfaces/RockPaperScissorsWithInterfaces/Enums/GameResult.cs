﻿

namespace RockPaperScissorsWithInterfaces
{
   public  enum GameResult
    {
        Win,
        Lose,
        Tie
    }
}
