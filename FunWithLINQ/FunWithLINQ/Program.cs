﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FunWithLINQ.Data;
using FunWithLINQ.Models;

namespace FunWithLINQ
{
    public class Program
    {
        static void Main(string[] args)
        {
            AnonymousTypes();
            Joins();
            GroupBy();

            Console.ReadLine();
        }

        static void AnonymousTypes()
        {
            Console.WriteLine("<= Anonymous Types");

            // create an instance of our repository
            StudentRepository repo = new StudentRepository();

            // get all the students
            List<Student> students = repo.GetAllStudents();

            // get all the famle students and return their name and major

            // Query Syntax
            //var ladies = from s in students     // iterate the collection of students
            //             where s.Gender == "F" // if Gender is gamle return student
            //             select new           // return a type I made up
            //             {
            //                 Name = $"{s.FirstName} {s.LastName}",s.Major
            //             };

            // Method Syntax
            var ladies = students.Where(s => s.Gender == "F").Select(x => new {Name = $"{x.FirstName} {x.LastName}",
                newID = x.Id*15, x.Major});

            // write out the list
            foreach (var lady in ladies)
            {
                Console.WriteLine($"{lady.Name} - {lady.Major} - {lady.newID}");
            }

            Console.WriteLine();
        }

        static void Joins()
        {
            Console.WriteLine("<= Joins");

            // create the repo
            var repo = new StudentRepository();

            // get your collections
            var students = repo.GetAllStudents();
            var courses = repo.GetAllStudentCourses();

            // join students to their courses and write out the name and the course

            // Query Syntax
            //var results = from s in students                // Get all the students
            //              join c in courses                // Join the courses
            //                on s.Id equals c.StudentId    // Map Id to StudentId
            //              orderby  s.FirstName           // order the results by firstname
            //    select new
            //    {
            //        StudentName = $"{s.FirstName} {s.LastName}",
            //        c.CourseName
            //    };

            // Method Syntax
            var results = students.OrderBy(s => s.FirstName)
                .Join(courses, s => s.Id, c => c.StudentId, (student, course) => new
            {
                course.CourseName,
                StudentName = $"{student.FirstName} {student.LastName}"
            }); 



            foreach (var result in results)
            {
                Console.WriteLine($"{result.StudentName} is taking {result.CourseName}");
            }

            Console.WriteLine();
        }

        static void GroupBy()
        {
            Console.WriteLine("<= GroupBy");

            // create the repo
            var repo = new StudentRepository();

            // get the student data
            var students = repo.GetAllStudents();

            // return students grouped by their major

            // Query Syntax
            //var results = (from s in students
            //              where s.Major != "Chemistry"
            //              orderby s.Major descending, s.LastName
            //              group s by s.Major).Take(2);

            // Method Syntax
            var results = students.Where(s => s.Major != "Chemistry")
                .OrderByDescending(s => s.Major)
                .ThenByDescending(s => s.LastName)
                .GroupBy(s => s.Major);

            foreach (var group in results)
            {
                Console.WriteLine($"{group.Key}");

                foreach (var student in group)
                {
                   Console.WriteLine($"\t{student.FirstName} {student.LastName}"); 
                }
            }

            Console.WriteLine();
        }
    }
}
