﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FunWithLINQ.Models;

namespace FunWithLINQ.Data
{
    public class StudentRepository
    {
        // This is where the data lives in THIS application
        // Note: In-Memoru repository
        private List<Student> _students;
        private List<StudentCourse> _courses;

        // constructor
        // initialize our repository
        public StudentRepository()
        {
            _students = new List<Student>()
            {
                new Student() {Id = 1, FirstName = "Bart", LastName = "Simpson", Gender = "M", Major = "Chemistry"},
                new Student() {Id = 2, FirstName = "Lisa", LastName = "Simpson", Gender = "F", Major = "Political Science"},
                new Student() {Id = 3, FirstName = "Bugs", LastName = "Bunny", Gender = "M", Major = "Pyschology"},
                new Student() {Id = 4, FirstName = "Lola", LastName = "Bunny", Gender = "F", Major = "Political Science"}
            };

            _courses = new List<StudentCourse>()
            {
                new StudentCourse() {StudentId = 1, CourseName = "Math"},
                new StudentCourse() {StudentId = 2, CourseName = "Math"},
                new StudentCourse() {StudentId = 3, CourseName = "Math"},
                new StudentCourse() {StudentId = 4, CourseName = "Math"},
                new StudentCourse() {StudentId = 2, CourseName = "Government"},
                new StudentCourse() {StudentId = 3, CourseName = "Into to Programming"}
            };
        }

        // GetAll function
        public List<Student> GetAllStudents()
        {
            return _students;
        }

        // GetAll function
        public List<StudentCourse> GetAllStudentCourses()
        {
            return _courses;
        }
    }
}
