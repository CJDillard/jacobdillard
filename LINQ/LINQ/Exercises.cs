﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class Exercises
    {

        public void Ex01()
        {
            // 1. Find all products that are out of stock.
            var products = DataLoader.LoadProducts();

            //var results = from p in products
                //where p.UnitsInStock == 0
                //select p;

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex02()
        { 
            //2. Find all products that are in stock and cost more than 3.00 per unit.
            var products = DataLoader.LoadProducts();

            var results = from p in products
                where p.UnitPrice > 3.00M
                select p;
            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }

        }

        public void Ex03()
        {
            //3. Find all customers in Washington, print their name then their orders. (Region == "WA")
            var persons = DataLoader.LoadCustomers();

            var results = from p in persons
                where p.Region == "WA"
                select p;
            foreach (var people in results)
            {
                Console.WriteLine(people.CustomerID);
                foreach (var order in people.Orders)
                {
                    Console.WriteLine($"\t {order.OrderID}");
                }
            }

        }

        public void Ex04()
        {
            //4. Create a new sequence with just the names of the products.
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new {p.ProductName};
            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex05()
        {
            //5. Create a new sequence of products and unit prices where the unit prices are increased by 25%.
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new {p, AdjustedPrice = p.UnitPrice*1.25m};
                foreach (var product in results)
            {
                Console.WriteLine($"{product.p.ProductName} {product.p.UnitPrice} - {product.AdjustedPrice}");
            }

        }

        public void Ex06()
        {
            //Create a new sequence of just product names in all upper case.
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new { p.ProductName };
            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName.ToUpper());
            }

        }

        public void Ex07()
        {
            //7. Create a new sequence with products with even numbers of units in stock.
            var products = DataLoader.LoadProducts();

            var results = from p in products
                where p.UnitsInStock%2 == 0
                select new {p, EvenStock = p.UnitsInStock%2 == 0};

            foreach (var product in results)
            {
                Console.WriteLine($"{product.p.ProductName} {product.p.UnitsInStock} {product.EvenStock}");
            }
        }

        public void Ex08()
        {
            //8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new  {p, Price = p.UnitPrice};
            foreach (var product in results)
            {
                Console.WriteLine($"\t{product.p.ProductName}\t {product.p.Category}\t {product.Price}");
            }
        }

        public void Ex09()
        {
            Console.WriteLine("OMIT");
        }

        public void Ex10()
        {
            // //10.Select CustomerID, OrderID, and Total where the order total is less than 500.00.
            var persons = DataLoader.LoadCustomers();

            var results = from p in persons
                          from o in p.Orders
                          where o.Total < 500.00m
                          select new { p, o };
            //var results = persons.Select(c => c.Orders.Where(o => o.Total < 500.00m));

            foreach (var obj in results)
            {
                Console.WriteLine($"{obj.p.CustomerID} {obj.o.OrderID} {obj.o.Total}");
            }




        }

        public void Ex11()
        {
            //11.Write a query to take only the first 3 elements from NumbersA.
            var numbers = DataLoader.NumbersA;

            var results = numbers.Take(3);

            foreach (var number in results)
            {
                Console.WriteLine(number);
            }

        }

        public void Ex12()
        {
            //.12.Get only the first 3 orders from customers in Washington.
            var persons = DataLoader.LoadCustomers();

            var results = (from p in persons
                where p.Region == "WA"
                select p.Orders).Take(3);
            foreach (var person in results)
            {
                Console.WriteLine(person);
                //foreach (var orders in )
            }
           

        }

        public void Ex13()
        {
            //13. Skip the first 3 elements of NumbersA.
            var numbers = DataLoader.NumbersA;

            var results = numbers.Skip(3);

            foreach (var number in results)
            {
                Console.WriteLine(number);
            }

        }

        public void Ex14()
        {
            //14.Get all except the first two orders from customers in Washington.
            var persons = DataLoader.LoadCustomers();

            var results = persons.Where(p => p.Region == "WA");

            foreach (var people  in results)
            {
                Console.WriteLine(people.CustomerID);
                foreach (var o in people.Orders.Skip(2))
                {
                    Console.WriteLine("\t{0}", o.OrderID);
                }
                
            }

        }

        public void Ex15()
        {
            //15.Get all the elements in NumbersC from the 
            //  beginning until an element is greater or equal to 6.
            var numbers = DataLoader.NumbersC.TakeWhile(n => n <= 6);

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }


        }

        public void Ex16()
        {
            //16. Return elements starting from the beginning of NumbersC until a number 
            //is hit that is less than its position in the array.
            var numbers = DataLoader.NumbersC.TakeWhile(n => n > Array.IndexOf(DataLoader.NumbersC, n));

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }

        }

        public void Ex17()
        {
            //17.Return elements from NumbersC starting from the first element divisible by 3.
            var numbers = DataLoader.NumbersC.Where(n => n % 3 == 0);

            foreach (var print in numbers)
            {
                Console.WriteLine("{0}", print);
            }

        }

        public void Ex18()
        {
            //18. Order products alphabetically by name.
            var products = DataLoader.LoadProducts().OrderBy(p => p.ProductName);

            foreach (var print in products)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex19()
        {
            //19. Order products by UnitsInStock descending.
            var products = DataLoader.LoadProducts().OrderByDescending(p => p.ProductName);

            foreach (var print in products)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex20()
        {
            //Sort the list of products, first by category, and then by unit price, from highest to lowest
            var products = DataLoader.LoadProducts()
                .OrderBy(p => p.Category)
                .ThenBy(p => p.UnitPrice)
                .OrderByDescending(p => p.UnitPrice);

            foreach (var print in products)
            {
                Console.WriteLine("{0, -20}{1}", print.Category, print.UnitPrice);
            }
        }

        public void Ex21()
        {
            //21. Reverse NumbersC
            var numbers = DataLoader.NumbersC.Reverse();

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex22()
        {
            //22. Display the elements of NumbersC grouped by their remainder when divided by 5.
            //var numbers = DataLoader.NumbersC
            var numbers = DataLoader.NumbersC.GroupBy(n => n %5);

            foreach (var print in numbers)
            {
                Console.WriteLine("{0}", print.Key);
            }


        }

        public void Ex23()
        {
            //23. Display products by Category
            var products = DataLoader.LoadProducts();

            foreach (var print in products)
            {
                Console.WriteLine("{0, -35} - {1}", print.ProductName, print.Category);
            }
        }

        public void Ex24()
        {
            Console.WriteLine("OMIT");
        }

        public void Ex25()
        {
            //25.Create a list of unique product category names.
            var products = DataLoader.LoadProducts().Select(p => new {p.Category}).Distinct();

            foreach (var print in products)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex26()
        {
            //26. Get a list of unique values from NumbersA and NumbersB.
            var numbers = DataLoader.NumbersA.Union(DataLoader.NumbersB);

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex27()
        {
            //Get a list of the shared values from NumbersA and NumbersB.
            var numbers = DataLoader.NumbersA.Intersect(DataLoader.NumbersB);

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex28()
        {
            //28. Get a list of values in NumbersA that are not also in NumbersB.
            var numbers = DataLoader.NumbersA.Except(DataLoader.NumbersB);

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }


        }

        public void Ex29()
        {
            //29. Select only the first product with ProductID = 12 (not a list).
            var products = DataLoader.LoadProducts().Where(p => p.ProductID == 12).GroupBy(p => p.ProductID)
                .Select(p => p).First();

            foreach (var print in products)
            {
                Console.WriteLine(print.ProductID +"     "+ print.ProductName);
            }
        }

        public void Ex30()
        {
            //30. Write code to check if ProductID 789 exists.
            var products = DataLoader.LoadProducts().Where(p => p.ProductID == 789).FirstOrDefault();
            if (products != null)
            {
                Console.WriteLine("It exists");
            }
            else
            {
                Console.WriteLine("Product does not exist");
            }
           
        }

        public void Ex31()
        {
            //31.Get a list of categories that have at least one product out of stock.
            var products = DataLoader.LoadProducts().Where(p => p.UnitsInStock == 0)
                .Select(p => p.Category).Distinct();

            foreach (var print in products)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex32()
        {
            //32. Determine if NumbersB contains only numbers less than 9.
            var numbers = DataLoader.NumbersB.All(n => n < 9);

            if (numbers == true)
            {
                Console.WriteLine("Every number is less than 9");
            }
            else
            {
                Console.WriteLine("Every number is not less than 9");
            }
        }

        public void Ex33()
        {
            //33. Get a grouped list of products only for categories that have all of their 
            //products in stock.
            var products = DataLoader.LoadProducts().Where(p => p.UnitsInStock >= 0)
               .Select(p => p.Category).Distinct();

            foreach (var print in products)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex34()
        {
            //34. Count the number of odds in NumbersA.
            var numbers = DataLoader.NumbersA.Where(n => n %2 != 0);

            foreach (var print in numbers)
            {
                Console.Write(print);
            }
        }

        public void Ex35()
        {
            //35. Display a list of CustomerIDs and only the count of their orders.
            var customers = DataLoader.LoadCustomers();
            var orders = customers.Count;
            foreach (var print in customers)
            {
                Console.WriteLine(print.CustomerID);
                Console.WriteLine(print.Orders.Count());
            }

        }

        public void Ex36()
        {
            //36. Display a list of categories and the count of their products.
            var products = DataLoader.LoadProducts().GroupBy(p => p.Category);
            foreach (var print in products)
            {
                Console.WriteLine("{0}", print.Key);
                Console.WriteLine("{0}", print.Count());
            }
        }

        public void Ex37()
        {
            //37. Display the total units in stock for each category.
            var products = DataLoader.LoadProducts().GroupBy(p => p.Category)
                .Select(g => new {g, total = g.Sum(p => p.UnitsInStock)})
                .Select(r => new {r.g.Key, r.total});
            foreach (var print in products)
            {
                Console.WriteLine("{0, -25} {1, -5}", print.Key, print.total);
            }
        }

        public void Ex38()
        {
            //38. Display the lowest priced product in each category.
            var products = DataLoader.LoadProducts().GroupBy(p => p.Category)
                 .Select(g => new { g, total = g.Min(p => p.UnitPrice) })
                 .Select(r => new { r.g.Key, r.total });
            foreach (var print in products)
            {
                Console.WriteLine("{0, -25} {1, -5}", print.Key, print.total);
            }
        }

        public void Ex39()
        {
            //39. Display the highest priced product in each category.
            var products = DataLoader.LoadProducts().GroupBy(p => p.Category)
                 .Select(g => new { g, total = g.Max(p => p.UnitPrice) })
                 .Select(r => new { r.g.Key, r.total });
            foreach (var print in products)
            {
                Console.WriteLine("{0, -25} {1, -5}", print.Key, print.total);
            }
        }

        public void Ex40()
        {
            //40. Show the average price of product for each category.
            var products = DataLoader.LoadProducts().GroupBy(p => p.Category)
                 .Select(g => new { g, total = g.Average(p => p.UnitPrice) })
                 .Select(r => new { r.g.Key, r.total });
            foreach (var print in products)
            {
                Console.WriteLine("{0, -25} {1, -5}", print.Key, print.total);
            }
        }
    }
}
