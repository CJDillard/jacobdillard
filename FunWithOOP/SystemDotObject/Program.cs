﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemDotObject
{
    public class Program
    {
        static void Main(string[] args)
        {
            Point p1 =new Point(11,17);
            Console.WriteLine(p1);

            Point p2 = new Point(11, 17);
            Console.WriteLine(object.Equals(p1, p2));
            Console.WriteLine(p1.Equals(p2));
            Console.WriteLine(object.ReferenceEquals(p1, p2));

            Point p3 = p1.Copy();
            Console.WriteLine(object.Equals(p1, p3));
            Console.WriteLine(p1.Equals(p3));
            Console.WriteLine(object.ReferenceEquals(p1, p3));

            Console.ReadLine();
        }
    }
}
