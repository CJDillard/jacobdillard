﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemDotObject
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        // only constuctor for this object
        public Point(int x , int y)
        {
            this.X = x;
            this.Y = y;
        }

        // antime we give the object to print use this method
        public override string ToString()
        {
            return $"Point as ({X}, {Y})";
        }

        // we'll take over comparisons here
        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            Point otherPoint = (Point) obj;

            return (this.X == otherPoint.X) && (this.Y == otherPoint.Y);

        }

        // you can create a member-wise copy
        public Point Copy()
        {
            return (Point) this.MemberwiseClone();
        }
    }
}
