﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();
            Car car2 = new Car(200);

            MiniVan myVan = new MiniVan();
            Console.WriteLine(myVan.MaxSpeed);


            Console.ReadLine();
        }
    }
}
