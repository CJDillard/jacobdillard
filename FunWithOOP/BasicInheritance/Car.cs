﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    public class Car
    {
        //properties
        public int MaxSpeed { get; set; }
        public int MinSpeed { get; set; }

        // public property with a private field
        private int _currentSpeed;

        public int Speed
        {
            get { return _currentSpeed; }
            set
            {
                _currentSpeed = value;
                if (_currentSpeed > MaxSpeed)
                {
                    _currentSpeed = MaxSpeed;
                }
            }
        }

        // default constructor
        public Car()
        {
            MaxSpeed = 100;
            MinSpeed = 0;
        }

        // another consturctor
        public Car(int max)
        {
            MaxSpeed = max;
            MinSpeed = 0;
        }
    }
}
