﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public abstract class Shape
    {
        // virutal - this method can be overriden on the derived class
        public virtual string Draw()
        {
            return "Drawing a Shape";
        }

        public abstract double Area();

    }
}
