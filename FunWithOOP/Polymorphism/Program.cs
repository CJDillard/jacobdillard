﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class Program
    {
        static void Main(string[] args)
        {
            Circle myCircle = new Circle();
            Console.WriteLine(myCircle.Draw());
            Console.WriteLine(myCircle.Radius);

            Shape myShape = new Circle();
            Console.WriteLine(myShape.Draw());

            Triangle myTriangle = new Triangle();
            Console.WriteLine(myTriangle.Draw());

            Console.ReadLine();
        }
    }
}
