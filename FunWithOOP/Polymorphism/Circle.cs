﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public class Circle : Shape
    {
        // property only on a Cricle
        public double Radius { get; set; }

        public override string Draw()
        {
            return "Drawing a Circle";
        }

        public override double Area()
        {
            return Math.PI*Math.Pow(Radius, 2);
        }
    }


}
