﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Containers;
using RPG.Inventory.Weapons;
using RPG.Inventory.Weapons.EverythingElse;

namespace RPG.Inventory.Tests
{
    [TestFixture]
    public class SatchelTests
    {
        [Test]
        public void CanAddLittleItems()
        {
            Satchel bag = new Satchel();
            WoodenSword sword = new WoodenSword();

            bag.Add(sword);

            Assert.AreEqual(1, bag.ItemCount);

            bag.DisplayContents();
        }

        [Test]
        public void CannotAddBigItems()
        {
            Satchel bag = new Satchel();
            BattleAxe axe = new BattleAxe();

            bag.Add(axe);

            Assert.AreEqual(0, bag.ItemCount);

            bag.DisplayContents();
        }
    }
}
