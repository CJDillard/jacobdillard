﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Reagents.Healing;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.Tests
{
    [TestFixture]
    public class ReagentPouchTests
    {
        [Test]
        public void CanAddReagentToPouch()
        {
            ReagentPouch bag = new ReagentPouch();
            Mushroom shroom = new Mushroom();

            bag.Add(shroom);

            Assert.AreEqual(1, bag.ItemCount);

            bag.DisplayContents();
        }

        [Test]
        public void CannotAddNonReagents()
        {
            ReagentPouch bag = new ReagentPouch();
            WoodenSword sword = new WoodenSword();

            bag.Add(sword);

            Assert.AreEqual(0, bag.ItemCount);

            bag.DisplayContents();
        }
    }
}
