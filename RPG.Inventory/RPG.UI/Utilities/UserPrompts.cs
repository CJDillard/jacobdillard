﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.UI.Utilities
{
    public class UserPrompts
    {
        public static string GetStringFromUser(string prompt)
        {
            do
            {
                Console.Write(prompt);
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {

                    Console.WriteLine("\nYou must enter valid string!");
                    PressKeyToContinue();
                }
                else
                {
                    return input;
                }
            } while (true);
        }
        public static string GetYesNoAnswerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt + "(Y/N)? ");
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter Y/N");
                    PressKeyToContinue();
                }
                else
                {
                    input = input.ToUpper();
                    if (input != "Y" && input != "N")
                    {
                        Console.WriteLine("You must enter Y/N");
                        PressKeyToContinue();
                        continue;
                    }

                    return input;
                }
            }
        }
        public static int GetSetUpIntegerFromUser(string prompt)
        {
            bool isValid = false;
            
            do
            {
                Console.WriteLine("\n" + prompt);
                string input = Console.ReadLine();
                int output;
                isValid = int.TryParse(input, out output);

                if (isValid)
                {
                    if (output <= 4)
                    {
                        return output;
                    }
                }
                else
                {
                    Console.WriteLine("Please enter a number between 1-4...");
                }

            } while (!isValid);


            return 0;
        }
        public static void PressKeyToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
