﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Base
{
    public abstract class Reagent : Item
    {
        public int HealingValue { get; set; }
    }
}
