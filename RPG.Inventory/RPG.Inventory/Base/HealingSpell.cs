﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Inventory.Base
{
    public abstract class HealingSpell : Item
    {
        public int HealingValue { get; set; }
        public int MagicToUse { get; set; }
    }
}
