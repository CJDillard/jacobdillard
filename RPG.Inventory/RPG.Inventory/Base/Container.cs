﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Inventory.Base
{
    public abstract class Container : Item
    {
        protected int _capacity;
        protected Item[] _itemsInContainer;
        protected int _currentIndex;

        public Container(int capacity)
        {
            _capacity = capacity;
            _itemsInContainer = new Item[_capacity];
            _currentIndex = 0;
        }

        public virtual void Add(Item itemToAdd)
        {
            _itemsInContainer[_currentIndex] = itemToAdd;
            _currentIndex++;
        }

        public virtual Item RemoveItem()
        {
            Item itemToReturn = _itemsInContainer[_currentIndex - 1];
            _itemsInContainer[_currentIndex - 1] = null;
            _currentIndex--;
            return itemToReturn;
        }

        public virtual void DisplayContents()
        {
            foreach (var item in _itemsInContainer)
            {
                if (item != null)
                {
                    Console.WriteLine("\t{0} - {1}", item.Name, item.Description);
                }
            }


        }

        public int ItemCount
        {
            get { return _currentIndex; }
        }

    }
}
