﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RPG.Inventory.Base
{
    public class Gold : Item
    {
        public Gold()
        {
            this.Value = 10;
        }
    }

    //public class Program
    //{
    //    public static void Main()
    //    {
    //        var player = new Player();
    //        player.Items = new List<Item>();
    //        player.Items.Add(new Gold());
    //        Console.WriteLine(player.GetTotalValue());

    //    }
    //}
    //public class Player
    //{
    //    public List<Item> Items { get; set; }

    //    public int GetTotalValue()
    //    {
    //        return Items.Sum(i => i.Value);
    //    }
    //}
}
