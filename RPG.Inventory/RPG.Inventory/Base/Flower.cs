﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Inventory.Base
{
    public abstract class Flower: Reagent
    {
        public int DamageIncrease { get; set; }
        public int MagicIncrease { get; set; }
        public int DexterityIncrease { get; set; }
    }
}
