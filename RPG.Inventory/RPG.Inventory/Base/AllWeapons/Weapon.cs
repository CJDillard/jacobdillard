﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Armor.Shields;
using RPG.Inventory.Base;

namespace RPG.Inventory.Base
{
    public abstract class Weapon : Item
    {
        public int Damage { get; set; }
    }
}
