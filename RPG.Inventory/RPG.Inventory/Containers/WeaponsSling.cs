﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Containers
{
    public class WeaponSling : SpecificConatiner
    {
        public WeaponSling() : base(ItemType.Weapon, 3)
        {
            Name = "A basic weapon sling. A must need for any hero.";
            Description = "Too many Weapons. Not enough arms.";
            Weight = 1;
            Value = 50;
            Type = ItemType.Container;
        }
    }
}
