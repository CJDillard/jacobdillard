﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Containers
{
    public class ReagentPouch : SpecificConatiner
    {
        public ReagentPouch() : base(ItemType.Reagent, 6)
        {
            Name = "A small reagent pouch";
            Description = "For all your basic alchemy needs";
            Weight = 1;
            Value = 20;
            Type = ItemType.Container;
        }
    }
}
