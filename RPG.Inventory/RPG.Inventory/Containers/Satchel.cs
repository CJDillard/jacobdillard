﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Containers
{
    public class Satchel : Container
    {
        public Satchel() : base(5)
        {
            Name = "A small cloth satchel";
            Description = "Might as well get a backpack ";
            Weight = 1;
            Value = 30;
            Type = ItemType.Container;
        }

        public override void Add(Item itemToAdd)
        {
            if (itemToAdd.Weight > 5)
                Console.WriteLine("That is way too big for the satchel!");
            else
                base.Add(itemToAdd);
        }
    }
}
