﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Weapons.EverythingElse;

namespace RPG.Inventory.Containers
{
    public class Quiver : SpecificConatiner
    {
        public Quiver() : base(ItemType.Arrow, 30)
        {
            Name = "A quiver made from fur and leather.";
            Description = "Holds arrows. Self explanaitory";
            Weight = 1;
            Value = 25;
            Type = ItemType.Container;
        }
    }
}
