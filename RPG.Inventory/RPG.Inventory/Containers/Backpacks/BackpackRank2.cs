﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Containers
{
    public class BackpackRank2 : Container
    {
        public BackpackRank2() : base(16)
        {
            Name = "An upgraded backpack with double the capacity";
            Description = string.Format("This backpack has {0} slots.", _capacity);
            Weight = 8;
            Value = 350;
            Type = ItemType.Container;
        }
    }
}
