﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Containers
{
    public class BackpackRank3 : Container
    {
        public BackpackRank3() : base(24)
        {
            Name = "The highest ranking backpack there is. Ultimate storage.";
            Description = string.Format("This backpack has {0} slots.", _capacity);
            Weight = 10;
            Value = 500;
            Type = ItemType.Container;
        }
    }
}
