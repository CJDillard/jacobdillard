﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Reagents
{
    public class LargeMushroom : Reagent
    {
        public LargeMushroom()
        {
            Name = "Large red and yellow Mushroom";
            Description = "Returns a moderate amount of health";
            HealingValue = 30;
            Weight = 1;
            Value = 10;
            Type = ItemType.Reagent;
        }
    }
}
