﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Reagents.Healing
{
    public class SmallMushroom : Reagent
    {
        public SmallMushroom()
        {
            Name = "A small orange mushroom";
            Description = "Heals a small protion of health";
            HealingValue = 7;
            Weight = 0;
            Value = 3;
            Type = ItemType.Reagent;
        }
    }
}
