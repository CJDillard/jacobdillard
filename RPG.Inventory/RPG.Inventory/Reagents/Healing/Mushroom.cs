﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Reagents.Healing
{
    public class Mushroom : Reagent
    {
        public Mushroom()
        {
            Name = " A red mushroom with white spots";
            Description = "Heals a decent amount of health";
            HealingValue = 15;
            Weight = 0;
            Value = 5;
            Type = ItemType.Reagent;
        }
    }
}
