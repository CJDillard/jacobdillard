﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Reagents.StatBoosters
{
    public class YellowFlower : Flower
    {
        public YellowFlower()
        {
            Name = " A red mushroom with white spots";
            Description = "Heals a small protion of health";
            DexterityIncrease = 15;
            Weight = 0;
            Value = 5;
            Type = ItemType.Reagent;
        }
    }
}
