﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor
{
    public class IronShield : Armour
    {
        public IronShield()
        {
            Name = "Iron armour. Basic Armour for any wannabe warrior";
            Description = "Might have a few dents in it but will protect you from most damage.";
            DefenseValue = 7;
            Weight = 10;
            Value = 35;
            Type = ItemType.Armour;
        }
    }
}
