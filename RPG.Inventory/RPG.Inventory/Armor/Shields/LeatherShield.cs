﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor.Shields
{
    public class Leathershield : Armour
    {
        public Leathershield()
        {
            Name = "Leather wrapped over wood.";
            Description = "Useful for when you want to get maimed but not killed.";
            DefenseValue = 5;
            Weight = 6;
            Value = 20;
            Type = ItemType.Armour;
        }
    }
}
