﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor
{
    public class SteelShield : Armour
    {
        public SteelShield()
        {
            Name = "Shield made from Solid Steel ";
            Description = "Blocks just about anything. Just make sure you can actually pick up the thing.";
            DefenseValue = 10;
            Weight = 15;
            Value = 55;
            Type = ItemType.Armour;
        }
    }
}
