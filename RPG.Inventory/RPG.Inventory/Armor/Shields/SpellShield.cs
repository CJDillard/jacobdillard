﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor.Shields
{
    public class SpellShield : Armour
    {
        public SpellShield()
        {
            Name = "Magic Shield created by mages";
            Description = "Mages got tired of being killed so easily so " +
                          "they devised a spell that allows them to create a force field";
            DefenseValue = 6;
            Weight = 0;
            Value = 0;
            Type = ItemType.Armour;
        }
    }
}
