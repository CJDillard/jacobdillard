﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor
{
    public class SteelArmourSet : Armour
    {
        public SteelArmourSet()
        {
            Name = "Steel mined from ore and handcrafted in a forged.";
            Description = "You'll only see the finest warriors wearing this armour set. Nothing else compares.";
            DefenseValue = 50;
            Weight = 25;
            Value = 150;
            Type = ItemType.Armour;
        }
    }
}
