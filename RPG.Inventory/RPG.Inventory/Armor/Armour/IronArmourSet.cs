﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor
{
    public class IronArmourSet : Armour
        {
            public IronArmourSet()
            {
                Name = "Iron armour. Must have for any wannabe warrior";
                Description = "Might have a few dents in it but will protect you from any major damage.";
                DefenseValue = 20;
                Weight = 15;
                Value = 75;
                Type = ItemType.Armour;
        }
    }
}
