﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor
{
    public class ClothArmourSet : Armour
    {
        public ClothArmourSet()
        {
            Name = "Silk robes favored by mages and the like";
            Description = "Comes in many colors but the standard set is black and red.";
            DefenseValue = 10;
            Weight = 7;
            Value = 50;
            Type = ItemType.Armour;

        }
    }
}
