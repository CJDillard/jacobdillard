﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Armor
{
    public class LeatherArmourSet : Armour
    {
        public LeatherArmourSet()
        {
            Name = "Leather thats been dyed black and dark green. Favored by rogues and the like.";
            Description = "Allows for flexible movement and thats about it. ";
            DefenseValue = 10;
            Weight = 5;
            Value = 30;
            Type = ItemType.Armour;
        }
    }
}
