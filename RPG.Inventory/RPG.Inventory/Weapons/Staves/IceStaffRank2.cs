﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class IceStaffRank2 : Staves 
    {
        public IceStaffRank2()
        {
            Name = "Upgraded version of the base ice staff";
            Description = "Ice has started to grow around the staff making it cold to the touch";
            EleDamage = 10;
            Weight = 5;
            Value = 25;
            Type = ItemType.Weapon;
        }
    }
}
