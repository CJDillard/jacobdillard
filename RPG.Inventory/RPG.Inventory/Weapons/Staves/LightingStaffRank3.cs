﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class LightingStaffRank3 : Staves 
    {
        public LightingStaffRank3()
        {
            Name = "Final version of the lighting staff";
            Description = "Passerbys can't tell if theres a storm coming or if It's just you. The answer is both.";
            EleDamage = 13;
            Weight = 6;
            Value = 35;
            Type = ItemType.Weapon;
        }
    }
}
