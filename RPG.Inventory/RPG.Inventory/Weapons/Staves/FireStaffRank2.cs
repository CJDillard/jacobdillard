﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class FireStaffRank2 : Staves 
    {
        public FireStaffRank2()
        {
            Name = "Upgraded version of the base Fire staff";
            Description = "Its charred wood and scars make the staffs age noticeable";
            EleDamage = 10;
            Weight = 5;
            Value = 25;
            Type = ItemType.Weapon;
        }
    }
}
