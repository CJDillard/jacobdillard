﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class IceStaffRank3 : Staves 
    {
        public IceStaffRank3()
        {
            Name = "Final version of the ice staff";
            Description = "The staff is ice incarnate. Any and all foes will either run or die trying.";
            EleDamage = 13;
            Weight = 6;
            Value = 35;
            Type = ItemType.Weapon;
        }
    }
}
