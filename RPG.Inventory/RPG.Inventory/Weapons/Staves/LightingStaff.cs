﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class LightingStaff : Staves 
    {
        public LightingStaff()
        {
            Name = "Lighting staff enchanted in a lighting storm";
            Description = "Lighting crackles as pops as it shoots from the tip of this staff";
            EleDamage = 5;
            Weight = 4;
            Value = 15;
            Type = ItemType.Weapon;
        }
    }
}
