﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class LighitngStaffRank2 : Staves 
    {
        public LighitngStaffRank2()
        {
            Name = "Upgraded version of the base Lighting staff";
            Description = "Your hair stands up ever so slightly as you grip the staff and occasional sparks can be seen jumping off it";
            EleDamage = 10;
            Weight = 5;
            Value = 25;
            Type = ItemType.Weapon;
        }
    }
}
