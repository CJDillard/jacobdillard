﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class FireStaffRank3 : Staves 
    {
        public FireStaffRank3()
        {
            Name = "Final version of the fire staff";
            Description = "Legends say there has only ever been one other fire staff this powerful. Now theres two.";
            EleDamage = 13;
            Weight = 6;
            Value = 35;
            Type = ItemType.Weapon;
        }
    }
}
