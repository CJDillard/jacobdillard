﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class FireStaff : Staves
    {
        public FireStaff()
        {
            Name = "Fire staff enchanted near a volcano";
            Description = "Spits flames towards your foes";
            EleDamage = 5;
            Weight = 4;
            Value = 15;
            Type = ItemType.Weapon;
        }
    }
}
