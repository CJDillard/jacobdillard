﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons
{
    public class IceStaff : Staves 
    {
        public IceStaff()
        {
            Name = "Enchanted in ice caves deep below the surface";
            Description = "Shoots ice shard projectiles to impale your foes";
            EleDamage = 5;
            Weight = 4;
            Value = 15;
            Type = ItemType.Weapon;
        }
    }
}
