﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons.SpellScrolls
{
    public class Fireball : Spell
    {
        public Fireball()
        {
            Name = "Spell scroll that lets you shot flames from your hands.";
            Description = "It has burnt edges and its words give off an orange and red hue";
            EleDamage = 10;
            MagicToUse = 15;
            Weight = 1;
            Value = 15;
            Type = ItemType.Weapon;

        }
    }
}
