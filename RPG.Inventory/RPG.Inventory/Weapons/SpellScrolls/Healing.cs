﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons.SpellScrolls
{
    public class Healing : HealingSpell
    {
        public Healing()
        {
            Name = "Spell scroll that lets you heal for a small amount of health";
            Description = "The scrolls gives off an unatural, pleasant glow.";
            HealingValue = 15;
            MagicToUse = 7;
            Weight = 1;
            Value = 15;
            Type = ItemType.Weapon;

        }
    }
}
