﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons.SpellScrolls
{
    public class IceShard : Base.AllWeapons.Spell
    {
        public IceShard ()
        {
            Name = "Spell scroll that lets you imaple your foes with ice";
            Description = "Its cold to the touch and has a cost layer of frost on it.";
            EleDamage = 10;
            MagicToUse = 15;
            Weight = 1;
            Value = 15;
            Type = ItemType.Weapon;

        }
    }
}
