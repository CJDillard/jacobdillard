﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;

namespace RPG.Inventory.Weapons.SpellScrolls
{
    public class LightingBolt : Spell
    {
        public LightingBolt()
        {
            Name = "Spell scroll that lets the user show lighting from their fingertips";
            Description = "Sparks fly and jump off and the static shock one gets from touching it usually isn't worth it. ";
            EleDamage = 10;
            MagicToUse = 15;
            Weight = 1;
            Value = 15;
            Type = ItemType.Weapon;

        }
    }
}
