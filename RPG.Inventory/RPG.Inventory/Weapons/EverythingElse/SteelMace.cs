﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons.EverythingElse
{
    public class SteelMace : Weapon
    {
        public SteelMace()
        {
            Name = "Steel mace used to smash your enemies.";
            Description = "Hitting them in the face with it usually works";
            Damage = 12;
            Weight = 20;
            Value = 200;
            Type = ItemType.Weapon;
        }
    }
}
