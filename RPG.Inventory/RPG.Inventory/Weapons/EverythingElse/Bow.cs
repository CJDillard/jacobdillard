﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons.EverythingElse
{
   public class Bow : Weapon
    {
       public Bow()
       {
           Name = "Shoots arrows. Can't do much else.";
           Description = "Aim towards the enemy.";
           Damage = 5;
           Weight = 7;
           Value = 25;
           Type = ItemType.Weapon;
       }
    }
}
