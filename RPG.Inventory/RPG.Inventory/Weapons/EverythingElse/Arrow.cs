﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons.EverythingElse
{
    public class Arrow : Weapon
    {
        public Arrow()
        {
            Name = "A wooden arrow made for use in a Bow.";
            Description = "You could probably stab someone with it if you were commited enough.";
            Damage = 3;
            Weight = 1;
            Value = 3;
            Type = ItemType.Weapon;

        }

    }
}
