﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons.EverythingElse
{
    public class BattleAxe : Weapon
    {
        public BattleAxe()
        {
            Name = "A giant battleaxe";
            Description = "Favored by most Orcs";
            Damage = 15;
            Weight = 10;
            Value = 150;
            Type = ItemType.Weapon;
        }
    }
}
