﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class SteelSword : Weapon
    {
        public SteelSword()
        {
            Name = "Steel sword to slice your enemies to shreds";
            Description = "One of the finest blades money can buy or make";
            Damage = 11;
            Weight = 15;
            Value = 150;
            Type = ItemType.Weapon;
        }
    }
}
