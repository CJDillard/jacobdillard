﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class WoodenSword : Weapon
    {
        public WoodenSword()
        {
            Name = "A wooden sword";
            Description = "A sword made from common wood";
            Damage = 3;
            Weight = 4;
            Value = 10;
            Type = ItemType.Weapon;
        }
    }
}
