﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.Weapons
{
    public class IronDagger :Weapon
    {
        public IronDagger()
        {
            Name = "Small blade made from iron.";
            Description = "Remember adventure: Two is greater than one.";
            Damage = 4;
            Weight = 1;
            Value = 3;
            Type = ItemType.Weapon;
        }
    }
}
