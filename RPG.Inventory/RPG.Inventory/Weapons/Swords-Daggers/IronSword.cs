﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class IronSword : Weapon
    {
        public IronSword()
        {
            Name = "Sword made from iron. Probably better than the wooden sword.";
            Damage = 7;
            Description = "Stick them with the pointy end.";
            Weight = 10;
            Value = 50;
            Type = ItemType.Weapon;

        }
    }
}
