﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class SteelDagger : Weapon
    {
        public SteelDagger()
        {
            Name = "Forged steel with a sharp point.";
            Description = "Might not be a sword but, in the right hands it doesn't";
            Damage = 7;
            Weight = 5;
            Value = 50;
            Type = ItemType.Weapon;
        }
    }
}
