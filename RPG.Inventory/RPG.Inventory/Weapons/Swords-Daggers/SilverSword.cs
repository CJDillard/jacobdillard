﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class SilverSword :Weapon
    {
        public SilverSword()
        {
            Name = "Silver sword made for killing monsters";
            Description = "Legends say the monsters slayers of ole' used silver blades to hunt down their pray. You are no different.";
            Damage = 9;
            Weight = 15;
            Value = 100;
            Type = ItemType.Weapon;

        }
    }
}
