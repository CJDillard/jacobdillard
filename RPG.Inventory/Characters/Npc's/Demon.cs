﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Demon : Race
    {
        public Demon()
        {
            Name = "Demon";
            Description = "Its gray and charred skin are nothing easy to look at. Probably doesnt like you.";
            Health = 110;
            Magic = 50;
            Dexterity = 0;
            Hand2Hand = 4;
            Type = RaceType.HellSpawn;
        }
    }
}
