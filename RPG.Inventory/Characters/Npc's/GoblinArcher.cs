﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class GoblinArcher : Race
    {
        public GoblinArcher()
        {
            Name = "Goblin Archer";
            Description = "Just as annoying as its brethern, but these have range. ";
            Health = 70;
            Magic = 0;
            Dexterity = 35;
            Hand2Hand = 1;
            Type = RaceType.Goblin;
        }
    }
}
