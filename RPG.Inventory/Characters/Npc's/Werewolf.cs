﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Werewolf : Race
    {
        public Werewolf()
        {
            Name = "Werewolf";
            Description = "They walk on their hind legs and have razor shap claws. A perversion of nature.";
            Health = 90;
            Magic = 0;
            Dexterity = 0;
            Hand2Hand = 8;
            Type = RaceType. Werewolf;
        }
    }
}
