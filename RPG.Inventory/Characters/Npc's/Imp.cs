﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Imp : Race
    {
        public Imp()
        {
            Name = "Imp";
            Description = "Their small size and leathery wings give them away. Usually run in packs and/or with demons.";
            Health = 65;
            Magic = 55;
            Dexterity = 0;
            Hand2Hand = 0;
            Type = RaceType.HellSpawn;
        }
    }
}
