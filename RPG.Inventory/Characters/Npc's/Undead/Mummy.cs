﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Mummy : Race
    {
        public Mummy()
        {
            Name = "Mummy";
            Description = "Usually found in ancient tombs and caves. Sometimes they get lost and find themselves outside.";
            Health = 50;
            Magic = 0;
            Dexterity = 0;
            Hand2Hand = 6;
            Type = RaceType.Undead;
        }
    }
}
