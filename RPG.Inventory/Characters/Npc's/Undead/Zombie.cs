﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Zombie : Race
    {
        public Zombie()
        {
            Name = "Zombie";
            Description = "Reanimted corpse with an endless hunger. Slow and stupid but dangerous in packs. ";
            Health = 85;
            Magic = 0;
            Dexterity = 0;
            Hand2Hand = 7;
            Type = RaceType.Undead;
        }
    }
}
