﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Skeleton : Race
    {
        public Skeleton()
        {
            Name = "Skeleton";
            Description = "Found just about everywhere. Even on the main road. More common at night traveling in packs of 2-3.";
            Health = 90;
            Magic = 0;
            Dexterity = 0;
            Hand2Hand = 3;
            Type = RaceType.Undead;
        }
    }
}
