﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Vampire : Race
    {
        public Vampire()
        {
            Name = "Vampire";
            Description = "Old and powerful beings. Can control fire and the undead. " +
                          "Be warned traveler for mercy is not in their vocabulary";
            Health = 120;
            Magic = 60;
            Dexterity = 0;
            Hand2Hand = 3;
            Type = RaceType.Undead;
        }
    }
}
