﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class SkeletonArcher : Race
    {
        public SkeletonArcher()
        {
            Name = "Skeleton Archer";
            Description = "Even in death some warriors keep their fighting expertise. Be warned for they are highly skilled.";
            Health = 10;
            Magic = 0;
            Dexterity = 45;
            Hand2Hand = 2;
            Type = RaceType.Undead;
        }
    }
}
