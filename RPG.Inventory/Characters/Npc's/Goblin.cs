﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.Npc_s
{
    public class Goblin : Race
    {
        public Goblin()
        {
            Name = "Goblin";
            Description = "Small and green. Pests even to themselves.";
            Health = 80;
            Magic = 0;
            Dexterity = 20;
            Hand2Hand = 1;
            Type = RaceType.Goblin;
        }
    }
}
