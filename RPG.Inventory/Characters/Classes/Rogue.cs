﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Armor;
using RPG.Inventory.Armor.Shields;
using RPG.Inventory.Base;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents.Healing;
using RPG.Inventory.Weapons;

namespace Characters.Classes
{
    public class Rogue
    {
        public Satchel Satchel { get; set; }
        public Weapon WeaponHolding { get; set; }
        public Weapon WeaponHolding2 { get; set; }
        public Armour ArmourEquipped { get; set; }

        public Rogue()
        {
            Mushroom shroom = new Mushroom();
            Satchel.Add(shroom);

            IronDagger dagger = new IronDagger();
            WeaponHolding = dagger;

            IronDagger dagger2 = new IronDagger();
            WeaponHolding2 = dagger2;

            LeatherArmourSet armourSet = new LeatherArmourSet();
            ArmourEquipped = armourSet;
        }

    }
}
