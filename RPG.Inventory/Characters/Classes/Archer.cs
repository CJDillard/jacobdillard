﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Armor;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Reagents.Healing;
using RPG.Inventory.Weapons;
using RPG.Inventory.Weapons.EverythingElse;

namespace Characters.Classes
{
    public class Archer
    {
        public Satchel Satchel { get; set; }
        public Weapon WeaponHolding { get; set; }
        public Armour ArmourEquipped { get; set; }
        public Quiver QuiverEquipped { get; set; }

        public Archer()
        {
            Mushroom shroom = new Mushroom();
            Satchel.Add(shroom);

            Bow bow = new Bow();
            WeaponHolding = bow;

            Quiver quiver = new Quiver();
            QuiverEquipped = quiver;

            for (int i = 0; i < 20; i++)
            {
                Arrow arrow = new Arrow();
                QuiverEquipped.Add(arrow);
            }

            LeatherArmourSet armourSet = new LeatherArmourSet();
            ArmourEquipped = armourSet;

        }
    }
}
