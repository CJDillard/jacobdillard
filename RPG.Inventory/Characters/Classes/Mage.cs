﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Armor;
using RPG.Inventory.Armor.Shields;
using RPG.Inventory.Base;
using RPG.Inventory.Base.AllWeapons;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Reagents.Healing;
using RPG.Inventory.Weapons;

namespace Characters.Classes
{
    public class Mage
    {
        public Satchel Satchel { get; set; }
        public Weapon WeaponHolding { get; set; }
        public Armour WeaponHolding2 { get; set; }
        public Armour ArmourEquipped { get; set; }

        public Mage()
        {
            Mushroom shroom = new Mushroom();
            Satchel.Add(shroom);

            FireStaff staves = new FireStaff();
            WeaponHolding = staves;

            SpellShield shield = new SpellShield();
            WeaponHolding2 = shield;

            ClothArmourSet armourSet = new ClothArmourSet();
            ArmourEquipped = armourSet;

        }
    }
}
