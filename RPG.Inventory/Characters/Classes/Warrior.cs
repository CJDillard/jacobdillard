﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Armor;
using RPG.Inventory.Armor.Shields;
using RPG.Inventory.Base;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Reagents.Healing;
using RPG.Inventory.Weapons;

namespace Characters.Classes
{
    public class Warrior
    {
        public Satchel Satchel { get; set; }
        public Weapon WeaponHolding { get; set; }
        public Armour WeaponHolding2 { get; set; }
        public Armour ArmourEquipped { get; set; }

        public Warrior()
        {
            Mushroom shroom = new Mushroom();
            Satchel.Add(shroom);

            IronSword sword = new IronSword();
            WeaponHolding = sword;

            Leathershield shield = new Leathershield();
            WeaponHolding2 = shield;

            IronArmourSet armourSet = new IronArmourSet();
            ArmourEquipped = armourSet;
        }

    }
}
