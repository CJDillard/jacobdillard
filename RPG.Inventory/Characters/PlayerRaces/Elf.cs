﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.PlayerRaces
{
   public class Elf : Race
    {
       public Elf()
       {
           Name = "Elves";
           Description = "The Elves first migrated from the mountains. Known for their skills in magic and archery. " +
                         "They might seem soft but are from from it";
           Health = 75;
           Magic = 100;
           Dexterity = 75;
           Hand2Hand = 2;
            Type = RaceType.Elves;

       }
    }
}
