﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.PlayerRaces
{
    public class Orc : Race 
    {
        public Orc()
        {
            Name = "Orcs";
            Description =
                "Orcs are almost unstoppable with any weapon in their hands and if they don't have one their fists will " +
                "work just as well. First showed up on ships from their mainland";
            Health = 150;
            Magic = 25;
            Dexterity = 20;
            Hand2Hand = 5;
            Type = RaceType.Orcs;
        }
    }
}
