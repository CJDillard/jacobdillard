﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.PlayerRaces
{
    public class Dwarf : Race
    {
        public Dwarf()
        {
            Name = "Dwarf";
            Description = "Small and stout Dwarves are known for three things: mining, heroism, and drinking. Not necessarily in that order.";
            Health = 130;
            Magic = 0;
            Dexterity = 45;
            Hand2Hand = 3;
            Type = RaceType.Dwarves;
        }
    }
}
