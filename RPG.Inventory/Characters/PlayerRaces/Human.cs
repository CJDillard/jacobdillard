﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Characters.Base;

namespace Characters.PlayerRaces
{
  
  
    public class Human : Race 
    {
        public Human()
        {
            Name = "Humans";
            Description =
                "No one orignally knows where the first humans came from." +
                " Most legends say they always existed. Well rounded in all aspects of life.";
            Health = 100;
            Magic = 75;
            Dexterity = 55;
            Hand2Hand = 3;
            Type = RaceType.Human;
        }
    }
}
