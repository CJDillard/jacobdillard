﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Characters.Base
{
    public abstract class Race
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Health { get; set; }
        public int Magic { get; set; }
        public int Dexterity { get; set; }
        public int Hand2Hand { get; set; }
        public RaceType Type { get; set; }

        public Race()
        {
            Type = RaceType.Unknown;
        }

    }

    public enum RaceType
    {
        Unknown,
        Human,
        Dwarves,
        Elves,
        Orcs,
        Werewolf,
        Goblin,
        Undead,
        HellSpawn
    }
}
