﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelValidation.Models;

namespace ModelValidation.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult MakeBooking()
        {
            return View(new Appointment() {Date = DateTime.Now});
        }

        [HttpPost]
        public ViewResult MakeBooking(Appointment appt)
        {
            //if (string.IsNullOrEmpty(appt.ClientName))
            //{
            //    ModelState.AddModelError("ClientName", "Please enter your name");
            //}

            //if (ModelState.IsValidField("Date") && DateTime.Now > appt.Date)
            //{
            //    ModelState.AddModelError("Date", "You can't schedule an appointment in the past");
            //}

            //if (!appt.TermsAccepted)
            //{
            //    ModelState.AddModelError("TermsAccepted", "Resistance is futile!");
            //}

            //if (ModelState.IsValidField("Time") && appt.Time <= 0)
            //{
            //    ModelState.AddModelError("Time", "If you don't want the appointment dont book it!");
            //}

            //if (ModelState.IsValidField("ClientName") && ModelState.IsValidField("Date"))
            //{
            //    if (appt.ClientName == "Garfield" && appt.Date.DayOfWeek == DayOfWeek.Monday)
            //    {
            //        ModelState.AddModelError("","Garfield cannot book on Mondays!");
            //    }
            //}

            if (ModelState.IsValid)
            {
                // statement to store a new Appointment in a 
                // repository would go here in a real project

                return View("Completed", appt);
            }

            return View();
        }
    }
}