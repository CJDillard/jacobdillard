﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using ModelValidation.Models.Attributes;

namespace ModelValidation.Models
{
    //[NoGarfieldOnMondays(ErrorMessage = "Garfield, stop trying to book mondays!")]
    public class Appointment : IValidatableObject
    {
       //[Required]
        public string ClientName { get; set; }

        [DataType(DataType.Date)] 
        //[Required(ErrorMessage = "Pick a F#$%@^% date please!")]
        [FutureDate(ErrorMessage = "Please enter a date in the future!")]
        public DateTime Date { get; set; }

        //[Range(typeof(bool), "true", "true", ErrorMessage = "Do not resist!")]
        //[MustBeTrue(ErrorMessage = "This must be accepted!")]
        public bool TermsAccepted { get; set; }

        //[Range(typeof(decimal), "0.25", "8", ErrorMessage = "Must Specify duration")]
        public decimal Time { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(ClientName))
            {
                errors.Add(new ValidationResult("Please enter NAME", new []{"ClientName"}));
            }

            if (DateTime.Now > Date)
            {
                errors.Add(new ValidationResult("Need a date in the future", new[] { "Date" }));
            }

            if (errors.Count == 0 & ClientName == "Garfield" && Date.DayOfWeek == DayOfWeek.Monday)
            {
                errors.Add(new ValidationResult("No Mondays, Garfield!"));
            }

            if (!TermsAccepted)
            {
               errors.Add(new ValidationResult("Accept the Terms!", new [] {"TermsAccepted"}));
            }

            return errors;
        }
    }
}