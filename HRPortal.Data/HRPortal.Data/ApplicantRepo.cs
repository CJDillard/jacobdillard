﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HRPortal.Models;

namespace HRPortal.Data
{
    public class ApplicantRepo
    {
        private string _fileName = "DataFiles/applicants.txt";

        public ApplicantRepo()
        {
            if (HttpContext.Current != null)
            {
                _fileName = HttpContext.Current.Server.MapPath("~/" + _fileName);
            }
        }

        public List<Applicant> GetAll()
        {
            List<Applicant> allApplicants = new List<Applicant>();

            if (File.Exists(_fileName))
            {
                using (var reader = File.OpenText(_fileName))
                {
                    reader.ReadLine();

                    string inputLine;
                    while ((inputLine = reader.ReadLine()) != null)
                    {
                        var columns = inputLine.Split(',');
                        var applicant = new Applicant()
                        {
                            ApplicantId = int.Parse(columns[0]),
                            FirstName = columns[1],
                            LastName = columns[2],
                            PhoneNumber = int.Parse(columns[3]),
                            References = columns[4],
                            Address = new Address()
                            {
                                Street1 = columns[5],
                                Street2 = columns[6],
                                City = columns[7],
                                PostalCode = int.Parse(columns[8]),
                                State = new State()
                                {
                                    StateAbbreviation = columns[9],
                                    StateName = columns[10]
                                }
                            }
                        };

                        allApplicants.Add(applicant);
                    }
                }
            }

            return allApplicants;
        }
    }
}
