﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeReview2
{
    public class Hero
    {
        public string Picture { get; set; }
        public string HeroName { get; set; }
        public string HeroClass { get; set; }
        public string Weapon { get;set; }
        public int HeroId { get; set; }
        public string Summary {get; set;}
        public string Ability1 { get; set; }
        public string Ability2 { get; set; }
        public string Ability3 { get; set; }
        public string Ability4 { get; set; }
    }

    public class Character
    {
        public string CharacterName { get; set; }
        public string CharacterHouse { get; set; }
        public int CharacterId { get; set; }
    }
}
