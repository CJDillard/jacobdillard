﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CodeReview2
{
    class Program
    {
        static void Main(string[] args)
        {
            //OverwatchList();
            //GotDictionary();
            //DontHateMe();

            Console.ReadLine();

        }

        static void OverwatchList()
        {
            List<Hero> heroes = new List<Hero>()
            {
                new Hero() {HeroName = "Genji", HeroClass = "Offense", Weapon = "Shuriken", HeroId = 1},
                new Hero() {HeroName = "Mcree", HeroClass = "Offense", Weapon = "PeaceKeeper", HeroId = 2},
                new Hero() {HeroName = "Pharah", HeroClass = "Offense", Weapon = "Rocket Launcher", HeroId = 3},
                new Hero() {HeroName = "Reaper", HeroClass = "Offense",Weapon = "Hellfire Shotguns", HeroId = 4},
                new Hero() {HeroName = "Solider:76", HeroClass = "Offense", Weapon = "Heavy Pulse Rifle", HeroId = 5},
                new Hero() {HeroName = "Tracer", HeroClass = "Offense", Weapon = "Pulse Pistols", HeroId = 6},
                new Hero() {HeroName = "Bastion", HeroClass = "Defense", Weapon = "Submachine/Gatling Gun", HeroId = 7},
                new Hero() {HeroName = "Hanzo", HeroClass = "Defnese", Weapon = "Storm Bow", HeroId = 8},
                new Hero() {HeroName = "Junkrat", HeroClass = "Defense", Weapon = "Frag Launcher", HeroId = 9},
                new Hero() {HeroName = "Mei", HeroClass = "Defense", Weapon = "Endothermic Blaster", HeroId = 10},
                new Hero() {HeroName = "Trobjorn", HeroClass = "Defense", Weapon = "Rivet Gun", HeroId = 11},
                new Hero() {HeroName = "Widowmaker", HeroClass = "Defense", Weapon = "Widow's Kiss", HeroId = 12},
                new Hero() {HeroName = "D.Va", HeroClass = "Tank", Weapon = "Fusion Cannons or Light Gun", HeroId = 13},
                new Hero() {HeroName = "Reinhardt", HeroClass = "Tank", Weapon = "Rocket Hammer", HeroId = 14},
                new Hero() {HeroName = "Roadhog", HeroClass = "Tank", Weapon = "Scrap Gun and Chain Hook ", HeroId = 15},
                new Hero() {HeroName = "Winston", HeroClass = "Tank", Weapon = "Tesla Cannon", HeroId = 16},
                new Hero() {HeroName = "Zarya", HeroClass = "Tank", Weapon = "Particle Cannon", HeroId = 17},
                new Hero() {HeroName = "Ana", HeroClass = "Support", Weapon = "Biotic Rifle", HeroId = 18},
                new Hero() {HeroName = "Lucio", HeroClass = "Support", Weapon = "Sonic Amplifier", HeroId = 19},
                new Hero() {HeroName = "Mercy", HeroClass = "Support", Weapon = "Caduceus Staff/Pistol", HeroId = 20},
                new Hero() {HeroName = "Symmetra", HeroClass = "Support", Weapon = "Photon Projector", HeroId = 21},
                new Hero() {HeroName = "Zenyatta", HeroClass = "Support", Weapon = "Orb of Desturction", HeroId = 22},
            };
            foreach (var hero in heroes)
            {
                Console.WriteLine($"{hero.HeroName} is in the {hero.HeroClass} class and the weapon(s) they wield is/are the {hero.Weapon} while their Id is {hero.HeroId}\n");
            }
        }
        static void GotDictionary()
        {
            Dictionary<int, Character> characters = new Dictionary<int, Character>();
            Character c1 = new Character()
            {
                CharacterName = "Eddard Stark",
                CharacterHouse = "Stark",
                CharacterId = 1
            };
            Character c2 = new Character()
            {
                CharacterName = "Catelyn Stark",
                CharacterHouse = "Stark",
                CharacterId = 2
            };
            Character c3 = new Character()
            {
                CharacterName = "Robb Stark",
                CharacterHouse = "Stark",
                CharacterId = 3
            };
            Character c4 = new Character()
            {
                CharacterName = "Jon Snow",
                CharacterHouse = "Stark",
                CharacterId = 4
            };
            Character c5 = new Character()
            {
                CharacterName = "Sansa Stark",
                CharacterHouse = "Stark",
                CharacterId = 5
            };
            Character c6 = new Character()
            {
                CharacterName = "Brandon Stark",
                CharacterHouse = "Stark",
                CharacterId = 6
            };
            Character c7 = new Character()
            {
                CharacterName = "Arya Stark",
                CharacterHouse = "Stark",
                CharacterId = 7
            };
            Character c8 = new Character()
            {
                CharacterName = "Rickon Stark",
                CharacterHouse = "Stark",
                CharacterId = 8
            };
            Character c9 = new Character()
            {
                CharacterName = "Tywin Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 9
            };
            Character c10 = new Character()
            {
                CharacterName = "Kevan Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 10
            };
            Character c11 = new Character()
            {
                CharacterName = "Jamie Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 11
            };
            Character c12 = new Character()
            {
                CharacterName = "Cersei Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 12
            };
            Character c13 = new Character()
            {
                CharacterName = "Tyrion Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 13
            };
            Character c14 = new Character()
            {
                CharacterName = "Robert Baratheon",
                CharacterHouse = "Baratheon",
                CharacterId = 14
            };
            Character c15 = new Character()
            {
                CharacterName = "Stannis Baratheon",
                CharacterHouse = "Baratheon",
                CharacterId = 15
            };
            Character c16 = new Character()
            {
                CharacterName = "Renly Baratheon",
                CharacterHouse = "Baratheon",
                CharacterId = 16
            };

            characters.Add(c1.CharacterId, c1);
            characters.Add(c2.CharacterId, c2);
            characters.Add(c3.CharacterId, c3);
            characters.Add(c4.CharacterId, c4);
            characters.Add(c5.CharacterId, c5);
            characters.Add(c6.CharacterId, c6);
            characters.Add(c7.CharacterId, c7);
            characters.Add(c8.CharacterId, c8);
            characters.Add(c9.CharacterId, c9);
            characters.Add(c10.CharacterId, c10);
            characters.Add(c11.CharacterId, c11);
            characters.Add(c12.CharacterId, c12);
            characters.Add(c13.CharacterId, c13);
            characters.Add(c14.CharacterId, c14);
            characters.Add(c15.CharacterId, c15);
            characters.Add(c16.CharacterId, c16);

            foreach (var key in characters.Keys)
            {
                DisplayCharacter(characters[key]);
            }
        }
        static void DisplayCharacter(Character c)
        {
            Console.WriteLine($"{c.CharacterName} is affialted with House {c.CharacterHouse} and their Character Id {c.CharacterId}\n");
        }
        static void DontHateMe()
        {
            ArrayList cashMoney = new ArrayList();
            cashMoney.Add("Victor ");
            cashMoney.Add("loves ");
            cashMoney.Add("Jagged");
            cashMoney.Add(" Arrays.");

            string imSorry = "";

            foreach (object o in cashMoney)
            {
                imSorry += (string)o;
            }

            Console.WriteLine("{0}", imSorry);
            Console.ReadLine();
        }

        
    }

    
}
