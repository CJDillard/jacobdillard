﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GotDictionary
{
    class Program
    {
        public static void Main(string[] args)
        {
            GotDictionary();

            Console.ReadLine();
        }

         public static void GotDictionary()
        {
            Dictionary<int, Character> characters = new Dictionary<int, Character>();
            Character c1 = new Character()
            {
                CharacterName = "Eddard Stark",
                CharacterHouse = "Stark",
                CharacterId = 1
            };
            Character c2 = new Character()
            {
                CharacterName = "Catelyn Stark",
                CharacterHouse = "Stark",
                CharacterId = 2
            };
            Character c3 = new Character()
            {
                CharacterName = "Robb Stark",
                CharacterHouse = "Stark",
                CharacterId = 3
            };
            Character c4 = new Character()
            {
                CharacterName = " Jon Snow",
                CharacterHouse = "Stark",
                CharacterId = 4
            };
            Character c5 = new Character()
            {
                CharacterName = "Sansa Stark",
                CharacterHouse = "Stark",
                CharacterId = 5
            };
            Character c6 = new Character()
            {
                CharacterName = "Brandon Stark",
                CharacterHouse = "Stark",
                CharacterId = 6
            };
            Character c7 = new Character()
            {
                CharacterName = "Arya Stark",
                CharacterHouse = "Stark",
                CharacterId = 7
            };
            Character c8 = new Character()
            {
                CharacterName = "Rickon Stark",
                CharacterHouse = "Stark",
                CharacterId = 8
            };
            Character c9 = new Character()
            {
                CharacterName = "Tywin Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 9
            };
            Character c10 = new Character()
            {
                CharacterName = "Kevan Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 10
            };
            Character c11 = new Character()
            {
                CharacterName = "Jamie Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 11
            };
            Character c12 = new Character()
            {
                CharacterName = "Cersei Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 12
            };
            Character c13 = new Character()
            {
                CharacterName = "Tyrion Lannister",
                CharacterHouse = "Lannister",
                CharacterId = 13
            };
            Character c14 = new Character()
            {
                CharacterName = "Robert Baratheon",
                CharacterHouse = "Baratheon",
                CharacterId = 14
            };
            Character c15 = new Character()
            {
                CharacterName = "Stannis Baratheon",
                CharacterHouse = "Baratheon",
                CharacterId = 15
            };
            Character c16 = new Character()
            {
                CharacterName = "Renly Baratheon",
                CharacterHouse = "Baratheon",
                CharacterId = 16
            };

            characters.Add(c1.CharacterId, c1);
            characters.Add(c2.CharacterId, c2);
            characters.Add(c3.CharacterId, c3);
            characters.Add(c4.CharacterId, c4);
            characters.Add(c5.CharacterId, c5);
            characters.Add(c6.CharacterId, c6);
            characters.Add(c7.CharacterId, c7);
            characters.Add(c8.CharacterId, c8);
            characters.Add(c9.CharacterId, c9);
            characters.Add(c10.CharacterId, c10);
            characters.Add(c11.CharacterId, c11);
            characters.Add(c12.CharacterId, c12);
            characters.Add(c13.CharacterId, c13);
            characters.Add(c14.CharacterId, c14);
            characters.Add(c15.CharacterId, c15);
            characters.Add(c16.CharacterId, c16);

            foreach (var key in characters.Keys)
            {
                DisplayCharacter(characters[key]);
            }
        }

        public static void DisplayCharacter(Character c)
        {
            Console.WriteLine($"{c.CharacterName} is affialted with House {c.CharacterHouse}");
        }
    }
}
