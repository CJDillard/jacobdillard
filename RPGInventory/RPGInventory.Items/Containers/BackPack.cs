﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGInventory.Items.Containers
{
    public class BackPack : Container
    {
        public BackPack() : base (4)
        {
            Name = "A Leather backpack";
            Description = "Holds all your valuable loot";
            Weight = 2;
        }
    }
}
