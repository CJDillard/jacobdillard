﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGInventory.Items.Foods
{
    public class Ration : Item
    {
        public Ration()
        {
            Name = "Ration";
            Description = "Doesn't taste the best but gets the job done.";
            Weight = .5;
            Type = ItemType.Foods;
        }
    }
}
