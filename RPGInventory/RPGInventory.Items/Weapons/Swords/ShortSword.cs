﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGInventory.Items.Weapons.Swords
{
    public class ShortSword : Weapon, IAttackable
    {
        public ShortSword()
        {
            this.Name = "Short Sword";
            this.Description = "It might not be long but it's pointy...";
            this.Weight = 2;
        }

        public override string ToString()
        {
            return $"{Name} - {Description}\nType = {Type}";
        }

        public string Hit(string target)
        {
            return $"stabbed {target}";
        }

    }
}
