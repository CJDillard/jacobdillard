﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGInventory.Items.Weapons
{
    // this is the contract that I want weapons to follow 
    // in order for them to be used in combat
    public interface IAttackable
    {
        // any weapon that wants to be attackable must implement
        // this method 
        string Hit(string target);
    }
}
