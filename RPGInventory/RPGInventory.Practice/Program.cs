﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGInventory.Items.Containers;
using RPGInventory.Items.Foods;
using RPGInventory.Items.Potions;
using RPGInventory.Items.Weapons.Projectiles;
using RPGInventory.Items.Weapons.Swords;

namespace RPGInventory.Practice
{
    class Program
    {
        static void Main(string[] args)
        {
           //ShortSwordDemo();
           //PotionString();
           //ContainerCauseException();
           //ContainerFun();
           //ContainerFunUseException();
           //ContainerFunSpecificError();

            // demonstrate Dependency Injection
            DIExample();

           Console.ReadLine();
        }

        public static void ShortSwordDemo()
        {
            ShortSword mySword = new ShortSword();
            Console.WriteLine(mySword);
            mySword.Description = "You'll get more than a paper cut";
            Console.WriteLine(mySword);
        }

        public static void PotionString()
        {
            HealingPotion potion = new HealingPotion();
            Console.WriteLine(potion);
        }

        public static void ContainerCauseException()
        {
            BackPack myPack = new BackPack();
            myPack.AddItem(new HealingPotion());
            myPack.AddItem(new HealingPotion());
            myPack.AddItem(new HealingPotion());
            myPack.AddItem(new HealingPotion());
            myPack.AddItem(new HealingPotion());
        }

        public static void ContainerFun()
        {
            try
            {
               ContainerCauseException(); 
            }
            catch
            {
             Console.WriteLine("Not enough space!!");   
            }
        }

        public static void ContainerFunSpecificError()
        {
            try
            {
                ContainerCauseException();
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("You can't add that!");
            }
            catch
            {
                Console.WriteLine("Not enough space!");
            }

            Console.WriteLine("Can I keep running?");
        }

        public static void ContainerFunUseException()
        {
            try
            {
                ContainerCauseException();
            }
            catch (IndexOutOfRangeException iex)
            {
                Console.WriteLine(iex);
            }
            catch
            {
                Console.WriteLine("Not enough space!");
            }

            Console.WriteLine("Can I keep running?");
        }

        public static void DIExample()
        {
            Samurai jack = new Samurai(new ShortSword());
            Console.WriteLine(jack.Attack("the bad guy"));

            //if (jack.SecondaryWeapon != null)
            //{
                Console.WriteLine(jack.SecondaryAttack("the evil doers"));
            //}

            jack.SecondaryWeapon = new Shuriken();
            Console.WriteLine(jack.SecondaryWeapon);

            jack.PickUpItem(new ShortSword());
            jack.PickUpItem(new Shuriken());
            jack.PickUpItem(new HealingPotion());
            jack.PickUpItem(new Ration());
        }
    }
}
