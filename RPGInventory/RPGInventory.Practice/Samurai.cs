﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGInventory.Items;
using RPGInventory.Items.Weapons;

namespace RPGInventory.Practice
{
    public class Samurai
    {
        private readonly IAttackable _weapon;
        private List<Item> _items;

        // property injection
        //property might be set, might now...
        public IAttackable SecondaryWeapon { get; set; }

        // constructor injection
        // we are taking a concrete/specific object as input during creation
        public Samurai(IAttackable weapon)
        {
            _weapon = weapon;
            _items = new List<Item>();
        }

        public string Attack(string target)
        {
            // secondary weapon may not exist 
            // meed to check for null
            if (SecondaryWeapon != null)
            {
              return _weapon.Hit(target);
            }

            return "You do not have a secondary weapon";

        }

        public string SecondaryAttack(string target)
        {
            return SecondaryWeapon.Hit(target);
        }

        // method injection
        // be able to provide dependency in method call
        public void PickUpItem(Item item)
        {
            _items.Add(item);
        }
    }
}
