﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizer
{
    class Program
    {
        static void Main(string[] args)
        {
            int input;

            Console.Write("Give me a number, please: ");
            input = int.Parse(Console.ReadLine());
            int[] factors = new int[input];

            // find all factors and add to array
            for (int i = 1; i < input; i++)
            {
                if (input % i == 0)
                {
                    factors[i - 1] = i;
                    Console.WriteLine(i + " ");
                }
            }

            /*
                       [1, 2, 3, 5, 7, 8]
                       -- 0 1 2 3 4 5 --
            */
            // Test for Perfect
            int total = 0;
            for (var i = 0; i < factors.Length - 1; i++)
                total += factors[i];
            if (total == input)
                Console.WriteLine(input + " is a perfect number");
            else
                Console.WriteLine(input + " is not a perfect number");

            Console.WriteLine("The total of all factors of " + input + " is " + total + ".");

            //Test for Prime 
            if (total == 1)
                Console.WriteLine(input + " is a prime number");
            else
                Console.WriteLine(input + " is not a prime number.");

            Console.WriteLine("Press Enter to exit");
            Console.Read();
        }
    } 
}
