﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class Strings
    {
        //Exercise 1
        public string SayHi(string name)
        {
            return string.Format("Hello {0}!", name);
        }

        //Exercise 2
        public string Abba(string a, string b)
        {
            return string.Format("{0}{1}{1}{0}", a, b);

        }

        //Exercise 3
        public string MakeTags(string tag, string content)
        {
            return string.Format("<{0}>{1}</{0}>", tag, content);
        }

        //Exercise 4
        public string InsertWord(string container, string word)
        {
            return string.Format("{0}{1}{2}", container.Substring(0, 2), word, container.Substring(2));

        }

        //Exercise 5
        public string MultipleEndings(string str)
        {
            return string.Format("{0}{0}{0}", str.Substring(str.Length - 2));
        }

        //Exercise 6
        public string FirstHalf(string str)
        {
            return string.Format("{0}", str.Substring(0, str.Length/2));
        }

        //Exercise 7
        public string TrimOne(string str)
        {
            return string.Format("{0}", str.Substring(1, str.Length - 2));
        }

        //Exercise 8
        public string LongInMiddle(string a, string b)
        {
            string result;
            if (a.Length < b.Length)
                result = string.Format("{1}{0}{1}", b, a);
            else
                result = string.Format("{0}{1}{0}", b, a);
            return result;
        }

        //Exercise 9
        public string Rotateleft2(string str)
        {
            return string.Format("{0}{1}", str.Substring(2), str.Substring(0, 2));
        }

        //Exercise 10
        public string RotateRight2(string str)
        {
            return string.Format("{0}{1}", str.Substring(str.Length - 2), str.Substring(0, str.Length - 2));

        }

        //Exercise 11
        public string TakeOne(string str, bool fromFront)
        {
            if (fromFront)
                return string.Format("{0}", str.Substring(0, 1));
            else
                return string.Format("{0}", str.Substring(4));
        }

        //Exercise 12
        public string MiddleTwo(string str)
        {
            return string.Format(str.Substring(str.Length/2 - 1, 2));
        }

        //Exercise 13
        public bool EndsWithLy(string str)
        {
            if (str.Length < 2)
                return false;
            else if (str.Substring(str.Length - 2, 2) == "ly")
                return true;
            else
                return false;
        }

        //Exercise 14
        //public string FrontAndBack(string str, int n)
        //{
        //    string result;
        //    if (n <= 5)
        //    {
        //        result = str.Substring(0, 2);
        //    }
        //    else if (n >= 8)
        //    {
        //        result = str.Substring(0, 4);
        //    }
        //    else
        //    {
        //        result = str.Substring(n, 8);
        //    }
        //    return result;

        //}
        //Exercise 15
        public string TakeTwoFromPosition(string str, int n)
        {
            string result;
            if (n > str.Length - 2)
            {
                result = str.Substring(0, 2);
            }
            else if (n < 0)
            {
                result = str.Substring(0, 2);
            }
            else
            {
                result = str.Substring(n, 2);
            }
            return result;

        }

        //Exercise 16
        public bool HasBad(string str)
        {
            if (str.Length < 7)
                return true;

            else
                return false;
            //for (int i = 0; i < 2; i++)
            //{
            //    if (str.Substring(i, 3) == "bad")
            //    {
            //        return true;
            //    }
            //}
            //return false;
        }

        //Exercise 17
        public string AtFirst(string str)
        {
            string result;
            if (str.Length >= 2)
                result = string.Format("{0}", str.Substring(0, 2));
            else
                result = string.Format("{0}@", str.Substring(0));
            return result;

        }

        //Exericse 18
        public string LastChars(string str, string str2)
        {
            string result;
            if (str.Length >= 4)
                result = string.Format("{0}{1}", str.Substring(0, 1), str2.Substring(4));
            else if (str2.Length >= 2)
                result = string.Format("{0}{1}", str.Substring(0, 1), str2.Substring(3));
            else
                result = string.Format("{0}@", str.Substring(0, 1));
            return result;
        }

        //Exercise 19
        public string ConCat(string a, string b)
        {
            string result;
            if (a.Length > 0 && b.Length > 0 && a.Substring(a.Length - 1) == b.Substring(0, 1))
                result = string.Format("{0}{1}", a, b.Substring(1));
            else
                result = a + b;
            return result;
        }

        //Exercise 20
        public string SwapLast(string str)
        {
            string result;
            if (str.Length == 2)
                result = string.Format("{0}", str.Substring(0, str.Length - 2) + "ba");
            else if (str.Length == 3)
                result = string.Format("{0}", str.Substring(0, str.Length - 2) + "ta");
            else
                result = string.Format("{0}", str.Substring(0, str.Length - 2) + "gn");
            return result;
        }

        //Exercise 21 
        public bool FrontAgain(string str)
        {
            if (str.Length == 2)
                return true;
            else if (str.Length >= 5)
                return true;
            else
                return false;
        }

        //Exercise 22
        public string MinCat(string a, string b)
        {
            string result;
            if (b.Length == 2)
                result = string.Format("{0}{1}", a.Substring(3), b);
            else if (b.Length <= 4)
                result = string.Format("{0}{1}", a.Substring(1), b);
            else
                result = string.Format("{0}{1}", a, b.Substring(1));
            return result;
        }

        //Exercise 23
        public string TweakFront(string str)
        {
            string firstCharacter = "";
            string secondCharacter = "";
            if (str.Substring(0, 1) == "a")
                firstCharacter = "a";
            if (str.Substring(1, 1) == "b")
                secondCharacter = "b";
            return firstCharacter + secondCharacter + str.Substring(2);

        }
        //Exercise 24
        //public string StripX(string str)
        //{

        //    if (str.Substring(0, 1) == "x") ;

        //}
    }
}
