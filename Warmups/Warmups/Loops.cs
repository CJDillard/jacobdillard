﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
   public class Loops
    {
        //Exercise 1
        //Given a string and a non-negative int n, return a larger string that is n copies of the original string.
        public string StringTimes(string str, int n)
        {
            string result = "";
            for (int i = 1; i <= n; i++)
            {
                result += str;
            }
            return result;
        }
        //Exercise 2
        //Given a string and a non-negative int n, we'll say that the front of the string is the first 3 chars, 
        //or whatever is there if the string is less than length 3. 
        //Return n copies of the front;
        public string FrontTimes(string str, int n)
        {
            string result = "";
            for (int i = 1; i <= n; i++)
            {
                result += str.Substring(0,3);
            }
            return result;

        }
        //Exercise 3
        //Count the number of "xx" in the given string. We'll say that overlapping is allowed, so "xxx" contains 2 "xx". 
        public int CountXX(string str)
        {
            int xxCounter = 0;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i, 2) == "xx")
                {
                    xxCounter++;
                }
            }
            return xxCounter;

        }
        //Exercise 4
        //Given a string, return true if the first instance of "x" in the string is immediately followed by another "x". 
        public bool DoubleX(string str)
        {
           //if (0, str.Length - 2)
            {
                return true;
            }
            

        }

    }
}
