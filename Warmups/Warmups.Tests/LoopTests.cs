﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    public class LoopTests
    {
        [TestCase("Hi", 2, "HiHi")]
        [TestCase("Hi", 3, "HiHiHi")]
        [TestCase("Hi", 1, "Hi")]
        public void StringTimes(string str, int n, string result)
        {
            Loops loops = new Loops();
            string actual = loops.StringTimes(str, n);
            Assert.AreEqual(result, actual);
        }

        [TestCase("Chocolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        [TestCase("Abc", 3, "AbcAbcAbc")]
        public void FrontTimes(string str, int n, string result)
        {
            Loops loops = new Loops();
            string actual = loops.FrontTimes(str, n);
            Assert.AreEqual(result, actual);
        }
        [TestCase("abcxx", 1)]
        [TestCase("xxx", 2)]
        [TestCase("xxxx", 3)]
        public void CountXX(string str, int result)
        {
            Loops loops = new Loops();
            int actual = loops.CountXX(str);
            Assert.AreEqual(result, actual);

        }
        [TestCase("axxbb", true)]
        [TestCase("axaxxax", false)]
        [TestCase("xxxxx", true)]
        public void DoubleX(string str, bool result)
        {
            Loops loops = new Loops();
            bool actual = loops.DoubleX(str);
            Assert.AreEqual(result, actual);
        }

    } 
}
