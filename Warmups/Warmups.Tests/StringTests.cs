﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    public class StringTests
    {
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHiTest(string name, string result)
        {
            // Arrange 
            Strings strings = new Strings();

            // Act
            string actual = strings.SayHi(name);

            // Assert
            Assert.AreEqual(result, actual);

        }

        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void AbbaTest(string a, string b, string result)
        {
            Strings strings = new Strings();
            string actual = strings.Abba(a, b);
            Assert.AreEqual(result, actual);
        }
        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]
        public void MakeTagsTest(string tag, string content, string result)
        {
            Strings strings = new Strings();
            string actual = strings.MakeTags(tag, content);
            Assert.AreEqual(result, actual);
        }
        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void InsertWordTest(string container, string word, string result)
        {
            Strings strings = new Strings();
            string actual = strings.InsertWord(container, word);
            Assert.AreEqual(result, actual);
        }

        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultipleEndingsTest(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.MultipleEndings(str);
            Assert.AreEqual(result, actual);
        }

        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalfTest(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.FirstHalf(str);
            Assert.AreEqual(result, actual);
        }

        [TestCase("Hello", "ell")]
        [TestCase("java", "av")]
        [TestCase("coding", "odin")]
        public void TrimOneTest(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.TrimOne(str);
            Assert.AreEqual(result, actual);
        }
        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void LongInMiddle(string a, string b, string result)
        {
            Strings strings = new Strings();
            string actual = strings.LongInMiddle(a, b);
            Assert.AreEqual(result, actual);
        }
        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateLeft2(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.Rotateleft2(str);
            Assert.AreEqual(result, actual);
        }
        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateRight2(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.RotateRight2(str);
            Assert.AreEqual(result, actual);

        }
        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]
        public void TakeOne(string str, bool fromFront, string result)
        {
            Strings strings = new Strings();
            string actual = strings.TakeOne(str, fromFront);
            Assert.AreEqual(result, actual);

        }
        [TestCase("string", "ri")]
        [TestCase("code", "od")]
        [TestCase("Practice", "ct")]
        public void MiddleTwo(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.MiddleTwo(str);
            Assert.AreEqual(result, actual);
        }
        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("Oddy", false)]
        public void EndsWithLy(string str, bool result)
        {
            Strings strings = new Strings();
            bool actual = strings.EndsWithLy(str);
            Assert.AreEqual(result, actual);

        }

        //[TestCase("Hello", 2, "Helo")]
        //[TestCase("Chocolate", 3, "Choate")]
        //[TestCase("Chocolate", 1, "Ce")]
        //public void FrontAndBack(string str, int n, string result)
        //{
        //    Strings strings = new Strings();
        //    string actual = strings.FrontAndBack(str, n);
        //    Assert.AreEqual(result, actual);
        //}

        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]
        [TestCase("java", 3, "ja")]
        public void TakeTwoFromPosition(string str, int n, string result)
        {
            Strings strings = new Strings();
            string actual = strings.TakeTwoFromPosition(str, n);
            Assert.AreEqual(result, actual);
        }
        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbadxx", false)]
        public void HasBad(string str, bool result)
        {
            Strings strings = new Strings();
            bool actual = strings.HasBad(str);
            Assert.AreEqual(result, actual);
        }
        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]
        [TestCase("h", "h@")]
        public void AtFirst(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.AtFirst(str);
            Assert.AreEqual(result, actual);
        }
        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("hi","", "h@")]
        public void LastChars(string str, string str2, string result)
        { 
            Strings strings = new Strings();
            string actual = strings.LastChars(str, str2);
            Assert.AreEqual(result, actual);
        }
        [TestCase("abc", "cat", "abcat")]
        [TestCase("dog", "cat", "dogcat")]
        [TestCase("abc", "", "abc")]
        public void ConCat(string a, string b, string result)
        {
            Strings strings = new Strings();
            string actual = strings.ConCat(a, b);
            Assert.AreEqual(result, actual);
        }
        [TestCase("coding", "codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]
        public void SwapLast(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.SwapLast(str);
            Assert.AreEqual(result, actual);
        }

        [TestCase("edited", true)]
        [TestCase("edit", false)]
        [TestCase("ed", true)]
        public void FrontAgain(string str, bool result)
        {
            Strings strings = new Strings();
            bool actual = strings.FrontAgain(str);
            Assert.AreEqual(result, actual);
        }

        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void MinCat(string a, string b, string result)
        {
            Strings strings = new Strings();
            string actual = strings.MinCat(a, b);
            Assert.AreEqual(result, actual);
        }

        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
        [TestCase("abed", "abed")]
        public void TweakFront(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.TweakFront(str);
            Assert.AreEqual(result, actual);
        }

        //[TestCase("xHix", "Hi")]
        //[TestCase("xHi", "Hi")]
        //[TestCase("Hxix", "Hxi")]
        //public void StripX(string str, string result)
        //{
        //    Strings strings = new Strings();
        //    string actual = strings.StripX(str);
        //    Assert.AreEqual(result, actual);
        //}



    }
}
