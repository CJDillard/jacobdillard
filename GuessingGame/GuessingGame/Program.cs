﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter your name now, player: ");
            string name = Console.ReadLine();

            bool playAgain = false;
            do
            {
                Program something = new Program();
                something.PlayGame(name);

                Console.WriteLine("Do you want to play again?(Y/N)");
                string response = Console.ReadLine();

                switch (response.ToUpper())
                {
                    case "Y":
                    case "YES":
                        playAgain = true;
                        break;
                    default:
                        playAgain = false;
                        break;
                }
            } while (playAgain);

        }

        public void PlayGame(string name)
        {
            Random rndm = new Random();
            int numberToBeGuessed = rndm.Next(1, 21);
            int firstNumberGuessed = 0;


            bool isValid = false;
            do
            {
                Console.Write($"Enter a number between 1 and 20, {name} (q to quit): ");
                string input = Console.ReadLine();
                int numberGuessed;
                isValid = int.TryParse(input, out numberGuessed);


                if (isValid) // we have a number, it might be between 1-20, it might now...
                {
                    if (firstNumberGuessed == 0)
                    {
                        firstNumberGuessed = numberGuessed;
                    }

                    if (firstNumberGuessed == numberToBeGuessed)
                    {
                        Console.WriteLine($"You got it first try, {name}! Congrats!");
                        break;
                    }
                    else if (!(numberGuessed <= 20 && numberGuessed >= 1))
                    {
                        Console.WriteLine($"The number needs to be between 1 and 20, {name}");
                        isValid = false;
                    }
                    else
                    {
                        if (numberGuessed == numberToBeGuessed)
                        {
                            Console.WriteLine($"You win, {name}!");
                            break;
                        }
                        else
                        {
                            Console.WriteLine($"You lost, {name}. Try again!");
                            isValid = false;
                        }
                    }
                }
                else // did they type q?
                {
                    if (input.ToUpper() == "Q")
                    {
                        return;
                    }

                    Console.WriteLine("REALLY!? I said numbers! Preferably 1-20");
                }

            } while (!isValid);
        }
    }
}
