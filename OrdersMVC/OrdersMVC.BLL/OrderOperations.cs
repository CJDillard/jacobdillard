﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrdersMVC.Models;
using OrdersMVC.Data.FileRepositories;

namespace OrdersMVC.BLL
{
    public class OrderOperations
    {
        public List<Order> GetAllOrders()
        {
            var repo = new OrderFileRepository();
            return repo.GetAll();

        }

        public Order GetOrderById(int id)
        {
            var repo = new OrderFileRepository();
            return repo.GetById(id);
        }

        public List<State> GetAllStates()
        {
            var repo = new StateFileRepository();
            return repo.GetAll();
        }

        public void Add(Order order)
        {
            // ternary operator is saying:
            // if there are any contacts return the max contact id and add 1 to set our new contact id
            // else set to 1
            var orders = GetAllOrders();
            order.Id = (orders.Any()) ? orders.Max(c => c.Id) + 1 : 1;

            var repo = new OrderFileRepository();

            repo.Add(order);
        }
    }
}
