﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new Program();
            //prog.IterateString();
            //prog.SplitString();
            //prog.SimpleArray();
            //prog.ReverseString();
            //prog.DeclareImplicitArrays();
            prog.RectMultiDimensionalArray();
            //prog.JaggedMultiDimensionalArry();


            Console.ReadLine();
        }

        public void IterateString()
        {
            string s1 = "This is a string of characters.\tmore";

            foreach (char c in s1)
            {
                Console.WriteLine(c);
            }

            Console.WriteLine(s1.Length);
        }

        public void SplitString()
        {
            string[] words = "This is a sentence.".Split(' ');

            foreach (string word in words)
            {
                Console.WriteLine(word);
            }
        }

        public void SimpleArray()
        {
            int[] myInts = new int[3];
            myInts[0] = 100;
            myInts[2] = 300;

            // += at the end will skip iterating through certain items 
            //(fewer than n times through looping)
            for (int i = 0; i < myInts.Length; i += 2)
            {
                // same as saying i+=2 above
                //as long as there is no else statement beblow
                //if (i%2 == 0)
                //{
                Console.WriteLine(myInts[i]);
                //}

            }
        }

        public void ReverseString()
        {
            string myString = "Cash Money";

            for (int i = 0; i < myString.Length; i++)
            {
                Console.Write(myString[myString.Length - i - 1]);
            }

            Console.WriteLine();

            for (int i = myString.Length - 1; i >= 0; i--)
            {
                Console.Write(myString[i]);
            }
        }

        public void DeclareImplicitArrays()
        {
            var a = new[] {1, 10, 100, 1000};
            Console.WriteLine($"a is a: {a.ToString()}");

            var b = new[] {2, 20.9, 200, 2000};
            Console.WriteLine($"b is: {b.ToString()}");

            foreach (int item in a)
            {
                Console.Write($"{item}\t");
            }

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = (int) b[i];
            }

            foreach (int item in a)
            {
                Console.Write($"{item}\t");
            }
        }

        public void RectMultiDimensionalArray()
        {
            int[,] grid = new int[5, 10];

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    grid[i, j] = i*j;
                    Console.Write($"{grid[i, j]}\t");
                }
                Console.WriteLine();
            }
        }

        public void JaggedMultiDimensionalArry()
        {
            int[][] jagger = new int[5][];

            for (int i = 0; i < 5; i++)
            {
                jagger[i] = new int[i + 3];
            }

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < jagger[i].Length; j++)
                {
                    jagger[i][j] = i*j;
                    Console.Write($"{jagger[i][j]}\t");
                }
                Console.WriteLine();
            }
        }


    }
}
