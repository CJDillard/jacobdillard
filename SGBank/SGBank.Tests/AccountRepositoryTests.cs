﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.Tests
{
    [TestFixture]
   public class AccountRepositoryTests
    {
        [Test]
        public void WithdrawMoney()
        {
            InMemAccountRepository repo = new InMemAccountRepository();
            var account = new Account() {AccountNumber = "123456"};
            Assert.IsTrue(repo.Withdrawal(account, 100m));
        }
    }
}
