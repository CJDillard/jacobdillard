﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.BLL
{
    public class AccountOperations
    {
        public bool MakeWithdrawl(Account account, decimal amountToWithdraw)
        {
            var isSuccessful = false;

            var repo = AccountFactory.CreateAccountRepository();

            var source = repo.GetAccountByNumber(account.AccountNumber);
            if (source != null)
            {
                if (source.Balance >= amountToWithdraw)
                {
                    isSuccessful = repo.Withdrawal(source, amountToWithdraw);
                }
            }

            return isSuccessful;
        }

        public bool MakeDeposit(Account account, decimal amountToDeposit)
        {
            var isSuccessful = false;

            var repo = AccountFactory.CreateAccountRepository();

            var source = repo.GetAccountByNumber(account.AccountNumber);
            if (source != null)
            {
                isSuccessful = repo.Deposit(source, amountToDeposit);
            }

            return isSuccessful;
        }
    }
}
