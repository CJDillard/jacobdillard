﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories.Mock
{
   public class MockStateRepository : IState
   {
       private static List<State> _states;

       static MockStateRepository()
       {
           _states = new List<State>()
           {
               new State() {StateAbbreviation = "OH", StateName = "OHIO", TaxRate = 5.75m},
               new State() {StateAbbreviation = "AL", StateName = "ALABAMA", TaxRate = 4.00M},
               new State() {StateAbbreviation = "TN", StateName = "TENNESSEE", TaxRate = 6.25M}
           };
       }
       public List<State> ListStates()
       {
           return _states;
       }

       public State GetState(string stateAbbreviation)
       {
           return _states.FirstOrDefault(a => a.StateAbbreviation == stateAbbreviation);
       }
    }
}
