﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories.Mock
{
    public class MockOrderRepository : IOrder
    {
        private static List<Order> _orders;

        static MockOrderRepository()
        {
            _orders = new List<Order>()
            {
                new Order()
                {
                    OrderNumber = 1,
                    OrderDate = DateTime.Parse("10/10/2016"),
                    CustomerName = "VICTOR",
                    StateName = "OH",
                    TaxRate = 0.0575M,
                    ProductType = "CARPET",
                    OrderArea = 22.5M,
                    CostPerSquareFoot = 1.85M,
                    LaborCostPerSquareFoot = 2.50M,
                    MaterialCost = 41.63M,
                    LaborCost = 56.25M,
                    OrderTax = 5.63M,
                    OrderTotalCost = 103.51M
                },
                new Order()
                {
                    OrderNumber = 2,
                    OrderDate = DateTime.Parse("10/10/2016"),
                    CustomerName = "JACOB",
                    StateName = "TN",
                    TaxRate = 0.07M,
                    ProductType = "CERAMIC",
                    OrderArea = 50.00M,
                    CostPerSquareFoot = 4.25M,
                    LaborCostPerSquareFoot = 3.75M,
                    MaterialCost = 212.50M,
                    LaborCost = 187.50M,
                    OrderTax = 25.00M,
                    OrderTotalCost = 425.00M
                },
                new Order()
                {
                    OrderNumber = 3,
                    OrderDate = DateTime.Parse("10/10/2016"),
                    CustomerName = "SARAH",
                    StateName = "AL",
                    TaxRate = 0.04M,
                    ProductType = "CONCRETE",
                    OrderArea = 12.25M,
                    CostPerSquareFoot = 2.25M,
                    LaborCostPerSquareFoot = 3.50M,
                    MaterialCost = 27.56M,
                    LaborCost = 42.88M,
                    OrderTax = 2.82M,
                    OrderTotalCost = 73.26M
                }
            };
        }

        public List<Order> ListOrders(DateTime orderDate)
        {
            return _orders;
        }

        public Order GetOrder(int orderNumber)
        {
            return _orders.FirstOrDefault(a => a.OrderNumber == orderNumber);
        }

        public void AddOrder(Order order)
        {
            _orders.Add(order);
        }

        public void EditOrder(Order order, DateTime originalOrder)
        {
            var orderToUpdate = _orders.First(a => a.OrderNumber == order.OrderNumber);
            orderToUpdate.CustomerName = order.CustomerName;
            orderToUpdate.StateName = order.StateName;
            orderToUpdate.TaxRate = order.TaxRate;
            orderToUpdate.ProductType = order.ProductType;
            orderToUpdate.OrderArea = order.OrderArea;
            orderToUpdate.CostPerSquareFoot = order.CostPerSquareFoot;
            orderToUpdate.LaborCostPerSquareFoot = order.LaborCostPerSquareFoot;
            orderToUpdate.MaterialCost = order.MaterialCost;
            orderToUpdate.LaborCost = order.LaborCost;
            orderToUpdate.OrderTax = order.OrderTax;
            orderToUpdate.OrderTotalCost = order.OrderTotalCost;
        }

        public void RemoveOrder(DateTime date, int orderNumber)
        {
            _orders.Remove(_orders.FirstOrDefault(a => a.OrderNumber == orderNumber));
        }

        public void LogErrors(string message)
        {
            throw new NotImplementedException();
        }
    }
}
