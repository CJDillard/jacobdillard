﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories.Mock
{
    public class MockProductRepository: IProduct
    {

        private static List<Product> _products;

        static MockProductRepository()
        {
            _products = new List<Product>()
            {
                new Product() {ProductType = "CERAMIC", CostPerSquareFoot = 2.25M, LaborCostPerSquareFoot = 3.50M},
                new Product() {ProductType = "CONCRETE", CostPerSquareFoot = 4.25M, LaborCostPerSquareFoot = 3.75M},
                new Product() {ProductType = "CARPET", CostPerSquareFoot = 1.85M, LaborCostPerSquareFoot = 2.50M}
            };
        }
        public List<Product> ListProducts()
        {
            return _products;
        }

        public Product GetProduct(string productType)
        {
            return _products.FirstOrDefault(a => a.ProductType == productType);
        }
    }
}
