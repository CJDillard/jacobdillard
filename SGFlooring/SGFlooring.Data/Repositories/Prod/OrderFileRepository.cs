﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories.Prod
{
    public class OrderFileRepository : IOrder
    {
        private List<string> _orderFiles = new List<string>();
        private string _orderdirectory = @"Files\Orders\";
        public OrderFileRepository()
        {
            _orderFiles = ListOrdersInDirectory();
        } 

        public List<string> ListOrdersInDirectory()
        {
            DirectoryInfo dir = new DirectoryInfo(_orderdirectory);
            foreach (var file in dir.GetFiles("*.txt"))
            {
                _orderFiles.Add(file.Name);
            }
            return _orderFiles;
        }
        public List<Order> ListOrders(DateTime orderDate)
        {
            string orderFile = ConvertDatetoFile(orderDate);
            List<Order> ordersList = new List<Order>();

            if (_orderFiles.Contains(orderFile))
            {
                var rows = File.ReadAllLines(_orderdirectory + orderFile);

                for (int i = 1; i < rows.Length; i++)
                {
                    var columns = rows[i].Split(',');

                    var order = new Order();
                    order.OrderNumber = int.Parse(columns[0]);
                    order.OrderDate = DateTime.Parse(columns[1]);
                    order.CustomerName = columns[2].ToUpper();
                    order.StateName = columns[3].ToUpper();
                    order.ProductType = columns[4].ToUpper();
                    order.OrderArea = decimal.Parse(columns[5]);

                    if (order.CustomerName.Contains('|'))
                    {
                        order.CustomerName = order.CustomerName.Replace('|', ',');
                    }
                    ordersList.Add(order);
                }
            }
            return ordersList;
        }

        public Order GetOrder(int orderNumber)
        {
            throw new NotImplementedException();
        }

        public void AddOrder(Order order)
        {
            var orders = ListOrders(order.OrderDate);
            if (order.CustomerName.Contains(','))
            {
                order.CustomerName = order.CustomerName.Replace(',', '|');
            }
            orders.Add(order);
            WriteToFile(orders, order.OrderDate);
            
        }

        public void EditOrder(Order order, DateTime originalOrder)
        {
            var orders = ListOrders(order.OrderDate);
            Order orderToEdit = orders.Find(o => o.OrderNumber == order.OrderNumber);
            if (order.OrderDate == originalOrder)
            {
                orders.Remove(orderToEdit);
                orders.Add(order);
                WriteToFile(orders, order.OrderDate);
            }
            if (order.OrderDate != originalOrder)
            {
                var oldFile = ListOrders(originalOrder);
                var orderToRemove = oldFile.Find(o => o.OrderNumber == order.OrderNumber);
                oldFile.Remove(orderToRemove);
                orders.Add(order);
                WriteToFile(oldFile, originalOrder);
                WriteToFile(orders, order.OrderDate);
            }
        }

        public void RemoveOrder(DateTime date, int orderNumber)
        {
            var orders = ListOrders(date);
            Order removeOrder = new Order();
            foreach (var order in orders)
            {
                if (order.OrderNumber == orderNumber)
                {
                    removeOrder = order;
                }
            }
            orders.Remove(removeOrder);
           WriteToFile(orders, date);
        }

        public void LogErrors(string message)
        {
            DateTime timestamp = DateTime.Now;
            using (StreamWriter sw = new StreamWriter(@"C:\Users\Apprentice\Documents\_repos\charles.dillard.self.work\SGFlooring\SGFlooring.UI\bin\Debug\Files\ErrorLog.txt", true))
            {
                sw.WriteLine($"{timestamp:u}-{message}");
            }
        }

        public string ConvertDatetoFile(DateTime orderDate)
        {
            string orderFile = "Orders_" + orderDate.ToString("MMddyyyy") + ".txt";
            return orderFile;
        }

        public void WriteToFile(List<Order> orders, DateTime date)
        {
            string orderFileName = ConvertDatetoFile(date);

            if (orders.Count == 0)
            {
                File.Delete(_orderdirectory + orderFileName);
            }
            else
            {
                File.Delete(_orderdirectory + orderFileName);

                using (var writer = File.CreateText(_orderdirectory + orderFileName))
                {
                    writer.WriteLine("OrderNumber,OrderDate,CustomerName,StateName,ProductType,OrderArea");
                    foreach (var order in orders)
                    {
                        writer.WriteLine($"{order.OrderNumber},{order.OrderDate},{order.CustomerName},{order.StateName},{order.ProductType},{order.OrderArea}");
                    }
                }
            }
        }
    }
}
