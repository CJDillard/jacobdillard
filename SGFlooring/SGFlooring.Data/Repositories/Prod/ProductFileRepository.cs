﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories.Prod
{
    public class ProductFileRepository: IProduct
    {
        private string _directory = @"C:\Users\Apprentice\Documents\_repos\charles.dillard.self.work\SGFlooring\SGFlooring.UI\Files\Product.txt";
        public List<Product> ListProducts()
        {
            List<Product> products = new List<Product>();

            using (StreamReader sr = File.OpenText(_directory))
            {
                string inputLine = "";
                sr.ReadLine();
                while ((inputLine = sr.ReadLine()) != null)
                {
                    string[] inputParts = inputLine.Split(',');
                    Product product = new Product()
                    {
                        ProductType = inputParts[0].ToUpper(),
                        CostPerSquareFoot = decimal.Parse(inputParts[1]),
                        LaborCostPerSquareFoot = decimal.Parse(inputParts[2])

                    };
                    products.Add(product);
                }
                return products;
            }
        }

        public Product GetProduct(string productType)
        {
            throw new NotImplementedException();
        }
    }
}
