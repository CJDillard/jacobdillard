﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.Data.Interfaces
{
    public interface IOrder
    {
        List<Order> ListOrders(DateTime orderDate);
        Order GetOrder(int orderNumber);
        void AddOrder(Order order);
        void EditOrder(Order order, DateTime originalOrder);
        void RemoveOrder(DateTime date, int orderNumber);
        void LogErrors(string message);
    }
}
