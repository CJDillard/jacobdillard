﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.Data.Interfaces
{
    public interface IState
    {
        List<State> ListStates();
        State GetState(string stateAbbreviation);
    }
}
