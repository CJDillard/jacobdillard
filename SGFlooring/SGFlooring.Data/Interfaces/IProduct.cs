﻿using SGFlooring.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.Data.Interfaces
{
    public interface IProduct
    {
        List<Product> ListProducts();
        Product GetProduct(string productType);
    }
}
