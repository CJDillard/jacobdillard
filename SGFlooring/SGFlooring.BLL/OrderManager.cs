﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;
using SGFlooring.Models.Confirmation;

namespace SGFlooring.BLL
{
    public class OrderManager
    {
        private IOrder _order;

        public OrderManager()
        {
            _order = OrderRepositoryFactory.GetOrderRepository();
        }

        public Response<List<Order>> ListOrders(DateTime orderDate)
        {
            var result = new Response<List<Order>>();
            var orders = _order.ListOrders(orderDate);

            if (orders.Count == 0)
            {
                result.Success = false;
                result.Message = " No orders found for this date!";
                LogErrors("No orders found for this date!");
                
            }
            else
            {
                result.Data = orders;
                result.Success = true;
            }
            return result;
        }

        public Order PopulateNewOrder(Order newOrder)
        {
            newOrder.OrderNumber = GetNextOrderNumber(newOrder.OrderDate);
            Order CalculatedOrder = CalculateOrderValues(newOrder);
            return CalculatedOrder;
        }

        public Response<AddOrderConfirmation> AddOrder(Order newOrder)
        {
            var result = new Response<AddOrderConfirmation>();

            _order.AddOrder(newOrder);

            result.Success = true;
            result.Data = new AddOrderConfirmation()
            {
                OrderNumber = newOrder.OrderNumber,
                OrderDate = newOrder.OrderDate,
                CustomerName = newOrder.CustomerName
            };
            return result;
        }

        public Response<DeleteOrderConfirmation> RemoveOrder(DateTime date, int orderToDelete)
        {
            var result = new Response<DeleteOrderConfirmation>();
            _order.RemoveOrder(date, orderToDelete);

            result.Success = true;
            result.Data = new DeleteOrderConfirmation()
            {
                OrderNumber = orderToDelete,
                OrderDate = date,
            };

            return result;
        }

        public Response<EditOrderConfirmation> EditOrder(Order order, DateTime orderToEdit)
        {
            var result = new Response<EditOrderConfirmation>();

            _order.EditOrder(order, orderToEdit);

            result.Success = true;
            result.Data = new EditOrderConfirmation()
            {
                OrderDate = order.OrderDate,
                CustomerName = order.CustomerName,
                ProductType = order.ProductType,
                OrderArea = order.OrderArea,
                StateName = order.StateName
            };
            CalculateOrderValues(order);
            order.OrderNumber = GetNextOrderNumber(orderToEdit);
            return result;
        }

        private Order CalculateOrderValues(Order newOrder)
        {
            var stateManager = new StateManager();
            var productManager = new ProductManager();

            var state = stateManager.GetState(newOrder.StateName);

            newOrder.StateName = state.StateAbbreviation;
            newOrder.TaxRate = state.TaxRate;

            var product = productManager.GetProduct(newOrder.ProductType);

            newOrder.CostPerSquareFoot = product.CostPerSquareFoot;
            newOrder.LaborCostPerSquareFoot = product.LaborCostPerSquareFoot;

            newOrder.MaterialCost = newOrder.CostPerSquareFoot*newOrder.OrderArea;
            newOrder.LaborCost = newOrder.LaborCostPerSquareFoot*newOrder.OrderArea;
            decimal orderSubtotal = newOrder.MaterialCost + newOrder.LaborCost;
            newOrder.OrderTax = orderSubtotal*(newOrder.TaxRate/100);
            newOrder.OrderTotalCost = orderSubtotal + newOrder.OrderTax;

            return newOrder;
        }

        public int GetNextOrderNumber(DateTime newOrderDate)
        {
            List<Order> orders = _order.ListOrders(newOrderDate);

            if (orders.Count == 0)
            {
                return 1;
            }
            else
            {
                var orderNums = orders.OrderBy(o => o.OrderNumber).Select(o => o.OrderNumber);
                return Enumerable.Range(1, orderNums.Max() + 1).Except(orderNums).First();
            }
        }

        public static void LogErrors(string message)
        {
            IOrder repo = OrderRepositoryFactory.GetOrderRepository();

            repo.LogErrors(message);
        }
    }
}


