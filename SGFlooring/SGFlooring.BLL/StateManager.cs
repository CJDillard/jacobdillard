﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.BLL
{
    public class StateManager
    {
        private IState _state;

        public StateManager()
        {
            _state = StateRepositoryFactory.GetStateRepository();
        }

        public bool IsValidState(string stateInput)
        {
            bool result = false;
            var states = _state.ListStates();
            foreach (var state in states)
            {
                if (stateInput.ToUpper() == state.StateAbbreviation.ToUpper())
                {
                    result = true;
                }
                else if (stateInput == state.StateName)
                {
                    result = true;
                }
            }

            return result;
        }

        public State GetState(string stateName)
        {
            State matchedState = new State();
            var states = _state.ListStates();

            foreach (var state in states)
            {
                if (stateName == state.StateName)
                {
                    matchedState = state;
                }
                if (stateName == state.StateAbbreviation)
                {
                    matchedState = state;
                }
            }

            return matchedState;
        }


    }
}
