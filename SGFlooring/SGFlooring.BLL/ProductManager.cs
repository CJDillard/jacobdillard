﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.BLL
{
    public class ProductManager
    {
        private IProduct _product;

        public ProductManager()
        {
            _product = ProductRepositoryFactory.GetProductRepository();
        }

        public bool IsValidProduct(string productInput)
        {
            bool result = false;
            var products = _product.ListProducts();
            foreach (var prod in products)
            {
                if (productInput.ToUpper() == prod.ProductType.ToUpper())
                {
                    result = true;
                }
            }

            return result;
        }

        public Product GetProduct(string productType)
        {
            Product matchedProduct = new Product();
            var products = _product.ListProducts();

            foreach (var product in products)
            {
                if (productType == product.ProductType)
                {
                    matchedProduct = product;
                }
            }

            return matchedProduct;
        }
    }
}
