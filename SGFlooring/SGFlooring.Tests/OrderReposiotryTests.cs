﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Tests
{
    [TestFixture]
    public class OrderReposiotryTests
    {
        private IOrder repo;

        public OrderReposiotryTests()
        {
            repo = OrderRepositoryFactory.GetOrderRepository();
        }

        [Test]
        public void ListAllOrders()
        {
            var orders = repo.ListOrders(DateTime.Parse("10/10/2016"));

            Assert.AreEqual(4, orders.Count);
        }

        [Test]
        public void RemoveOrder()
        {
            DateTime time = new DateTime(2016, 10, 10);
            repo.RemoveOrder(time, 1);
            var deletedOrder = repo.GetOrder(1);
            Assert.IsNull(deletedOrder);
        }

        [TestCase(2, "JACOB", 50.00, 425.00)]
        public void SpeicifcOrder(int orderNumber, string customerName, decimal orderArea, decimal orderTotalCost)
        {
            var order = repo.GetOrder(orderNumber);
            Assert.AreEqual(customerName, order.CustomerName);
            Assert.AreEqual(orderArea, order.OrderArea);
            Assert.AreEqual(orderTotalCost, order.OrderTotalCost);
        }

        [TestCase(4, "CALLI", 50.00, 75.00)]
        public void AddOrder(int orderNumber, string customerName, decimal orderArea, decimal laborCostPerSquareFoot)
        {
            Order newOrder = new Order();
            newOrder.OrderNumber = orderNumber;
            newOrder.CustomerName = customerName;
            newOrder.OrderArea = orderArea;
            newOrder.LaborCostPerSquareFoot = laborCostPerSquareFoot;
            repo.AddOrder(newOrder);
            var orders = repo.ListOrders(DateTime.Parse("10/10/2016"));
            var addedOrder = repo.GetOrder(4);
            Assert.AreEqual(4, orders.Count);
            Assert.AreEqual(customerName, addedOrder.CustomerName);
            Assert.AreEqual(orderArea, addedOrder.OrderArea);
            Assert.AreEqual(laborCostPerSquareFoot, addedOrder.LaborCostPerSquareFoot);
        }

        [TestCase(3, "ZACH", 43.12, 55.66)]
        public void EditOrder(int orderNumber, string customerName, decimal orderArea,
            decimal materialCost)
        {
            Order editedOrder = repo.GetOrder(3);
            // to show that they do not match right now
            Assert.AreNotSame(customerName, editedOrder.CustomerName);
            editedOrder.OrderNumber = orderNumber;
            editedOrder.CustomerName = customerName;
            editedOrder.OrderArea = orderArea;
            editedOrder.MaterialCost = materialCost;
            repo.EditOrder(editedOrder, DateTime.Parse("10/11/16"));
            Order updatedOrder = repo.GetOrder(3);
            Assert.AreEqual(orderNumber, updatedOrder.OrderNumber);
            Assert.AreEqual(customerName, updatedOrder.CustomerName);
            Assert.AreEqual(orderArea, updatedOrder.OrderArea);
            Assert.AreEqual(materialCost, updatedOrder.MaterialCost);
        }




    }

}

    

            
  
