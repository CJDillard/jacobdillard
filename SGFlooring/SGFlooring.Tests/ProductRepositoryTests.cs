﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGFlooring.Data.Repositories.Mock;

namespace SGFlooring.Tests
{
    [TestFixture]
    public class ProductRepositoryTests
    {
        [Test]
        public void CanListProducts()
        {
            var repo = new MockProductRepository();
            var products = repo.ListProducts();
            Assert.AreEqual(3, products.Count);
        }
        
        [TestCase("CONCRETE", 4.25, 3.75)]
        public void SpecificProduct(string productType, decimal costPerSquareFoot, decimal laborCostPerSquareFoot)
        {
            var repo = new MockProductRepository();
            var product = repo.GetProduct("CONCRETE");
            Assert.AreEqual(product.ProductType, productType);
            Assert.AreEqual(product.CostPerSquareFoot, costPerSquareFoot);
            Assert.AreEqual(product.LaborCostPerSquareFoot, laborCostPerSquareFoot);
        }
    }
}
