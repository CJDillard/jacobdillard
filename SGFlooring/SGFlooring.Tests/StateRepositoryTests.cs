﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGFlooring.Data.Repositories.Mock;

namespace SGFlooring.Tests
{
    [TestFixture]
    public class StateRepositoryTests
    {
        [Test]
        public void CanListStates()
        {
            var repo = new MockStateRepository();
            var states = repo.ListStates();
            Assert.AreEqual(3, states.Count);
        }

        [TestCase("AL", "ALABAMA", 4.00)]
        public void SpecificState(string stateAbbreviation, string stateName, decimal taxRate)
        {
            var repo = new MockStateRepository();
            var state = repo.GetState("AL");
            Assert.AreEqual(state.StateAbbreviation, stateAbbreviation);
            Assert.AreEqual(state.StateName, stateName);
            Assert.AreEqual(state.TaxRate, taxRate);
        }
    }
}
