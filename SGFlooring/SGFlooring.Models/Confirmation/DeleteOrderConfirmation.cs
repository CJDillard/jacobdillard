﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGFlooring.Models.Confirmation
{
    public class DeleteOrderConfirmation
    {
        public int OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
