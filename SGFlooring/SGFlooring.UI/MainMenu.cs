﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.UI.Utilities;
using SGFlooring.UI.Workflows;

namespace SGFlooring.UI
{
    public class MainMenu
    {
        public void Execute()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Welcome to SG Flooring!");
                Console.WriteLine("*********************************************");
                Console.WriteLine("\n1. Display Orders");
                Console.WriteLine("2. Add New Order");
                Console.WriteLine("3. Edit An Order");
                Console.WriteLine("4. Remove An Order\n");
                Console.WriteLine("*********************************************");
                Console.WriteLine("(Q)uit");

                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");

                if (input.Substring(0, 1).ToUpper() == "Q")
                {
                    break;
                }
                else
                {
                    ProcessChoice(input);
                }
            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    DisplayOrdersWorkflow disOrder = new DisplayOrdersWorkflow();
                    disOrder.Execute();
                    break;
                case "2":
                    AddOrderWorkflow addOrder = new AddOrderWorkflow();
                    addOrder.Execute();
                    break;
                case "3":
                    EditOrderWorkflow ediOrder = new EditOrderWorkflow();
                    ediOrder.Execute();
                    break;
                case "4":
                    RemoveOrderWorkflow remOrder = new RemoveOrderWorkflow();
                    remOrder.Execute();
                    break;
            }
        }
    }
}
