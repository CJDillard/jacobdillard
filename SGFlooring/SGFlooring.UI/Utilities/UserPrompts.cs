﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGFlooring.UI.Utilities
{
    public class UserPrompts
    {
        public static int GetIntegerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write("\n" + prompt);
                string input = Console.ReadLine();
                int output;

                if (int.TryParse(input, out output))
                {
                    return output;
                }
                else
                {
                    Console.WriteLine("Please enter a valid number...");
                }

            }


        }

        public static decimal GetDecimalFromUser(string prompt)
        {
            do
            {
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                decimal output;

                if (decimal.TryParse(input, out output))
                {
                    return output;
                }

                PressKeyToContinue();

            } while (true);


        }

        public static string GetStringFromUser(string prompt)
        {
            do
            {
                Console.Write(prompt);
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {

                    Console.WriteLine("\nYou must enter valid string!");
                    PressKeyToContinue();
                }
                else
                {
                    return input;
                }
            } while (true);



        }

        public static DateTime GetDateFromUser(string prompt)
        {
            while (true)
            {
                Console.Write("\n" + prompt);
                string input = Console.ReadLine();

                DateTime result = new DateTime();

                DateTime.TryParse(input, out result);

                if (!result.Equals(DateTime.MinValue))
                {
                    return result;
                }
                else
                {
                    Console.WriteLine("Please enter a valid date...");
                }
            }

        }


        public static string GetYesNoAnswerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt + "(Y/N)? ");
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter Y/N");
                    PressKeyToContinue();
                }
                else
                {
                    input = input.ToUpper();
                    if (input != "Y" && input != "N")
                    {
                        Console.WriteLine("You must enter Y/N");
                        PressKeyToContinue();
                        continue;
                    }

                    return input;
                }
            }
        }

        public static void PressKeyToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

    }
}
