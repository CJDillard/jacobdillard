﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;
using SGFlooring.Models.Confirmation;

namespace SGFlooring.UI.Utilities
{
    public class DisplayScreens
    {
        private static string _seperatorBar = "==========================================================================";
        private const string _headerFormat = "{0, -10} {1, -20} {2, -17} {3, -10} {4, -10}";

        public static void DisplayOrdersList(List<Order> ordersList, DateTime orderDate)
        {
            Console.WriteLine("\nOrder for {0}: ", orderDate.ToString("d"));
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, "Order#", "OrderDate", "CustomerName", "State", "Product");
            Console.WriteLine(_seperatorBar);
            foreach (var order in
                ordersList)
            {
                Console.WriteLine(_headerFormat, order.OrderNumber, order.OrderDate.ToString("d"), order.CustomerName,
                    order.StateName, order.ProductType);
            }
        }

        public static void DisplayOrderDetails(Order order)
        {
            Console.WriteLine("Order To Be Added: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, "Order#", "OrderDate", "CustomerName", "State", "Product", "Order Total");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, order.OrderNumber, order.OrderDate.ToString("d"), order.CustomerName,
                order.StateName, order.ProductType, order.OrderTotalCost);
            Console.WriteLine("\n{0} sq. ft. @ {1} per sq. ft. = {2} in materials;", order.OrderArea.ToString(), order.CostPerSquareFoot.ToString("c"), order.MaterialCost.ToString("c"));
            Console.WriteLine("{0} sq. ft. @ {1} per sq. ft. = {2} in labor;", order.OrderArea.ToString(), order.LaborCostPerSquareFoot.ToString("c"), order.LaborCost.ToString("c"));
            Console.WriteLine("\nSub-total of {0} @ {1} % tax = {2} total tax;", (order.MaterialCost + order.LaborCost).ToString("c"), order.TaxRate.ToString(), order.OrderTax.ToString("c"));
            Console.WriteLine("Order Total: {0}", order.OrderTotalCost.ToString("c"));

        }

        public static void DisplayEditOrderDetails(Order order)
        {
            Console.WriteLine("Order To Be Edited: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, "Order#", "OrderDate", "CustomerName", "State", "Product", "Order Total");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, order.OrderNumber, order.OrderDate.ToString("d"), order.CustomerName,
                order.StateName, order.ProductType);

        }

        public static void EditedOrderDetails(Order editedOrder, EditOrderConfirmation editConfirmation)
        {
            Console.Clear();
            Console.WriteLine("New order info: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, "Order#", "OrderDate", "CustomerName", "State", "Product");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, editedOrder.OrderNumber, editedOrder.OrderDate.ToString("d"), editedOrder.CustomerName,
                editedOrder.StateName, editedOrder.ProductType);
        }

        public static void DisplayAddOrderConfirmationView(AddOrderConfirmation confirmation)
        {
            Console.WriteLine("\nThe following order has been added: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine("{0} - {1} - {2}", confirmation.OrderNumber, confirmation.OrderDate.ToString("d"), confirmation.CustomerName);

        }

        public static void AddOrderInputHeader()
        {
            Console.Clear();
            Console.WriteLine("Add Order: ");
            Console.WriteLine(_seperatorBar);
        }

        public static void RemoveOrderInputHeader()
        {
            Console.Clear();
            Console.WriteLine("Remove An Order: ");
            Console.WriteLine(_seperatorBar);
        }

        public static void DispplayDeleteOrderConfirmationView(DeleteOrderConfirmation orderConfirmation)
        {
            Console.WriteLine("\nThe following order has been deleted: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine("{0} - {1}", orderConfirmation.OrderDate.ToString("d"), orderConfirmation.OrderNumber);
            Console.WriteLine("{0} - {1}", orderConfirmation.OrderDate.ToString("d"), orderConfirmation.OrderNumber);
        }

        public static void EditOrderInputHeader()
        {
            Console.Clear();
            Console.WriteLine(_seperatorBar);
            Console.WriteLine("Edit An Order: ");

        }

        public static void OrderGoingToBeEdited(Order order)
        {
            Console.WriteLine("\nThe following will be edited: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, order.OrderNumber, order.OrderDate, order.CustomerName);
            Console.WriteLine("{0} - {1} - {2}", order.OrderNumber, order.OrderDate.ToString("d"), order.CustomerName);

        }
    }
}
