﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.Models;
using SGFlooring.Models.Confirmation;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class AddOrderWorkflow
    {
        Order newOrder = new Order();

        public void Execute()
        {
            var addOrderManager = new OrderManager();
            newOrder = GetAddOrderInput(newOrder);

            var confirmOrder = addOrderManager.PopulateNewOrder(newOrder);

            DisplayScreens.DisplayOrderDetails(confirmOrder);

            if (UserPrompts.GetYesNoAnswerFromUser("Confirm Order?").ToUpper() == "Y")
            {
                var result = addOrderManager.AddOrder(newOrder);
                AddOrderConfirmation orderConfirmation = result.Data;
                DisplayScreens.DisplayAddOrderConfirmationView(orderConfirmation);

            }

            else
            {
                Console.WriteLine("Your Order will not be placed");
            }

            UserPrompts.PressKeyToContinue();
        }

        private Order GetAddOrderInput(Order newOrder)
        {
            DisplayScreens.AddOrderInputHeader();
            newOrder.OrderDate = UserPrompts.GetDateFromUser("Enter order date (MM/DD/YYYY): ");
            newOrder.CustomerName = UserPrompts.GetStringFromUser("Enter customer name: ").ToUpper();
            newOrder.StateName = GetValidStateEntryFromUser();
            newOrder.ProductType = GetValidProductTypeFromUser();
            newOrder.OrderArea = UserPrompts.GetDecimalFromUser("Enter order area in square feet");
            return newOrder;
        }

        private string GetValidStateEntryFromUser()
        {
            while (true)
            {
                string stateInput = UserPrompts.GetStringFromUser("Enter state name (two-letter abbreviation): ").ToUpper();
                var stateManager = new StateManager();
                bool validatedStateInput = stateManager.IsValidState(stateInput);
                if (!validatedStateInput)
                {
                    Console.WriteLine("That is not a valid state. Please re-enter.\n");
                    
                }
                else
                {
                    return stateInput;
                }
            }
        }

        private string GetValidProductTypeFromUser()
        {
            while (true)
            {
                string productInput = UserPrompts.GetStringFromUser("Enter product type: ").ToUpper();
                var productManager = new ProductManager();
                bool validatedProductInput = productManager.IsValidProduct(productInput);
                if (!validatedProductInput)
                {
                    Console.WriteLine("That is not a valid product type. Please re-enter.\n");
                }
                else
                {
                    return productInput;
                }
            }
        }
    }
}
