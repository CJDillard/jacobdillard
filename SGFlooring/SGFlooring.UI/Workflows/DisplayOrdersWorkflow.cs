﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class DisplayOrdersWorkflow
    {
        public void Execute()
        {
            Console.Clear();
            DateTime addDate = UserPrompts.GetDateFromUser("Enter Order Date: ");
            Console.Clear();
            DisplayOrders(addDate);
        }

        private void DisplayOrders(DateTime orderDate)
        {
            var orderManager = new OrderManager();
            var result = orderManager.ListOrders(orderDate);
            if (result.Success)
            {
                DisplayScreens.DisplayOrdersList(result.Data, orderDate);
            }
            else
            {
                Console.WriteLine(result.Message);
            }
            UserPrompts.PressKeyToContinue();
        }
    }
}
