﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class RemoveOrderWorkflow
    {
        DateTime removeOrderDate = new DateTime();

        public void Execute()
        {
            var removeOrderManager = new OrderManager();
            DateTime removeOrder = GetRemoveOrderInput(removeOrderDate);
            var orderList = removeOrderManager.ListOrders(removeOrder);

            if (orderList.Success)
            {
                DisplayScreens.DisplayOrdersList(orderList.Data, removeOrder);
                int recordToDelete = UserPrompts.GetIntegerFromUser("Enter the Order Number of the order you want to delete: ");
                Console.WriteLine("Order Deleted");
                UserPrompts.PressKeyToContinue();
                removeOrderManager.RemoveOrder(removeOrder, recordToDelete);
            }
            else
            {
                Console.WriteLine(orderList.Message);
                Console.ReadLine();

            }
        }

        private DateTime GetRemoveOrderInput(DateTime date)
        {
            DisplayScreens.RemoveOrderInputHeader();
            date = UserPrompts.GetDateFromUser("Enter the date of the order you want to delete(MM/DD/YYYY): ");
            return date;
        }
    }
}
