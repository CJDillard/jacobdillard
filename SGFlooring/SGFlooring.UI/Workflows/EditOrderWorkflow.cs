﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.Models;
using SGFlooring.Models.Confirmation;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class EditOrderWorkflow
    {
        DateTime editOrderDate = new DateTime();
        public void Execute()
        {
            var editOrderManager = new OrderManager();
            int orderToEdit = 0;
            DateTime editOrder = OrderToEditDate(editOrderDate);
            var orderList = editOrderManager.ListOrders(editOrder);
            Order order = new Order();
            DateTime recordToEdit = new DateTime();
            if (orderList.Success)
            {
                DisplayScreens.DisplayOrdersList(orderList.Data, editOrder);
                orderToEdit = UserPrompts.GetIntegerFromUser("Enter the number of the order you want to edit: ");
                order = orderList.Data.Find(a => a.OrderNumber == orderToEdit);
                Console.Clear();
                DisplayScreens.DisplayEditOrderDetails(order);
                recordToEdit = order.OrderDate;
                order = PromptUserForUpdates(order);
                if (UserPrompts.GetYesNoAnswerFromUser("Confirm Edit?").ToUpper() == "Y")
                {
                    var result = editOrderManager.EditOrder(order, recordToEdit);
                    EditOrderConfirmation editConfirmation = result.Data;
                    DisplayScreens.EditedOrderDetails(order, editConfirmation);
                }
                else
                {
                    Console.WriteLine("Order will not be edited");
                }

            }
            else
            {
                Console.WriteLine(orderList.Message);
                Console.ReadLine();
            }
            UserPrompts.PressKeyToContinue();
        }

        private Order PromptUserForUpdates(Order order)
        {
            Order newOrder = new Order();
            newOrder.OrderNumber = order.OrderNumber;
            newOrder.OrderDate = EditDateForOrder(order.OrderDate,"Date");
            newOrder.CustomerName = PromptForStringValue(order.CustomerName, "Name: ").ToUpper();
            newOrder.StateName = GetValidStateEntryFromUser(order);
            newOrder.ProductType = GetValidProductTypeFromUser(order);
            newOrder.OrderArea = PromptForDecimalValue(order.OrderArea, "Area:");
            return newOrder;


        }

        private DateTime EditDateForOrder(DateTime currentDate, string field)
        {
            string newValue = "";
            DateTime newDate = new DateTime();

            Console.WriteLine($"{field} has current value of {currentDate}");
            Console.WriteLine("What is the new date you want for this order: ");
            newValue = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(newValue))
            {
                newDate = currentDate;
            }
            else
            {
                DateTime result = new DateTime();

                if (!DateTime.TryParse(newValue, out result))
                {
                    newDate = EditDateForOrder(currentDate, field);
                }
                else
                {
                    newDate = result;
                }
            }

            return newDate;
        }

        private decimal PromptForDecimalValue(decimal currentDecimal, string field)
        {
            string newValue = "";
            decimal newDecimal;

            Console.WriteLine($"{field} has current value of {currentDecimal}");
            Console.WriteLine("What is the new area you want for this order: ");
            newValue = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(newValue))
            {
                newDecimal = currentDecimal;
            }
            else
            {
                decimal result;

                if (!decimal.TryParse(newValue, out result))
                {
                    newDecimal = PromptForDecimalValue(currentDecimal, field);
                }
                else
                {
                    newDecimal = result;
                }
                    
            }
            return newDecimal;

        }

        private string PromptForStringValue(string currentValue, string field)
        {
            string newValue = "";

            Console.WriteLine($"{field} has current value of {currentValue}");
            Console.Write("What is the new value (enter to keep): ");
            newValue = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(newValue))
            {
                newValue = currentValue;
            }

            return newValue;
        }

        private string GetValidStateEntryFromUser(Order orderToEdit)
        {
            while (true)
            {
                var stateManager = new StateManager();
                string stateInput = PromptForStringValue(orderToEdit.StateName, "State: ").ToUpper();
                bool validatedStateInput = stateManager.IsValidState(stateInput);
                if (!validatedStateInput)
                {
                    Console.WriteLine("That is not a valid state. Please re-enter.\n");
                }
                else
                {
                    return stateInput;
                }
            }
        }

        private string GetValidProductTypeFromUser(Order orderToEdit)
        {
            while (true)
            {
                string productInput = PromptForStringValue(orderToEdit.ProductType, "Product: ");
                var productManager = new ProductManager();
                bool validatedProductInput = productManager.IsValidProduct(productInput);
                if (!validatedProductInput)
                {
                    Console.WriteLine("That is not a valid product type. Please re-enter.\n");
                }
                else
                {
                    return productInput;
                }
            }
        }

        private DateTime OrderToEditDate(DateTime date)
        {
            DisplayScreens.EditOrderInputHeader();
            date = UserPrompts.GetDateFromUser("Enter the date of the order you wish to edit: ");

            return date;
        }
    }
}
