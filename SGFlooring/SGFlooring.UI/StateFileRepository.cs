﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories.Prod
{
    public class StateFileRepository : IState
    {
        private string _directory = @"C:\Users\Apprentice\Documents\_repos\charles.dillard.self.work\SGFlooring\SGFlooring.UI\Files\States.txt";
        public List<State> ListStates()
        {
            List<State> states = new List<State>();
            using (StreamReader sr = File.OpenText(_directory))
            {
                string inputLine = "";
                sr.ReadLine();
                while ((inputLine = sr.ReadLine()) != null)
                {
                    string[] inputParts = inputLine.Split(',');
                    State state = new State()
                    {
                        StateAbbreviation = inputParts[0].ToUpper(),
                        StateName = inputParts[1].ToUpper(),
                        TaxRate = decimal.Parse(inputParts[2])
                    };
                    states.Add(state);
                }
                return states;
            }
        }

        public State GetState(string stateAbbreviation)
        {
            throw new NotImplementedException();
        }
    }
}
